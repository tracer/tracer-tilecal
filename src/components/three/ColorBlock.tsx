import { ChangeEvent } from "react";

type Props = {
  cellColor: string;
  value?: string;
  disabled?: boolean;
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
};

export default function ColorBlock({ cellColor, onChange, value, disabled }: Props) {
  return (
    <div className="flex select-none items-center gap-2 text-base  sm:text-xs">
      <div className="h-6 w-5" style={{ backgroundColor: cellColor }}></div>
      <input
        type="text"
        className="w-14 bg-transparent px-1 text-right outline-none"
        value={value || ""}
        disabled={disabled}
        onChange={onChange}
      />
      {value && <span>GeV</span>}
    </div>
  );
}
