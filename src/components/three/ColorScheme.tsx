import { ChangeEvent } from "react";

import { uid } from "#/utils/uid";
import {
  selectEnergySchemeIntervals,
  selectFixScale,
  setEnergySchemeIntervalMinMax,
  setFixScaleMode,
} from "#/store/features/tcalSlice";
import { useAppDispatch, useAppSelector } from "#/store/hooks";
import ColorBlock from "#/components/three/ColorBlock";
import { Checkbox } from "#/components/ui/checkbox";

export default function ColorScheme() {
  const dispatch = useAppDispatch();
  const energySchemeIntervals = useAppSelector(selectEnergySchemeIntervals);
  const fixScale = useAppSelector(selectFixScale);

  const minInterval = energySchemeIntervals[energySchemeIntervals.length - 1];
  const maxInterval = energySchemeIntervals[0];

  const updateMaxInterval = (e: ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value;

    dispatch(
      setEnergySchemeIntervalMinMax({
        min: minInterval.value,
        max: value,
      })
    );
  };

  const updateMinInterval = (e: ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value;

    dispatch(
      setEnergySchemeIntervalMinMax({
        min: value,
        max: maxInterval.value,
      })
    );
  };

  const toggleFixScale = (state: boolean) => {
    dispatch(setFixScaleMode(state));
  };

  const colorsBlocks = energySchemeIntervals.map(({ cellColor, value }, idx) => {
    if (idx === 0 || idx === energySchemeIntervals.length - 1) {
      return null;
    }

    return <ColorBlock key={uid()} cellColor={cellColor} value={value} disabled={true} />;
  });

  return (
    <div className="absolute right-0 top-20 z-20 mr-4 space-y-3 text-graywhite">
      <div className="flex w-full items-center justify-between gap-2 rounded-md border border-[#485058] p-2">
        <label
          htmlFor="terms"
          className="text-sm font-medium leading-none peer-disabled:cursor-not-allowed peer-disabled:opacity-70"
        >
          Fix Scale
        </label>
        <Checkbox onCheckedChange={toggleFixScale} checked={fixScale} id="terms" />
      </div>
      <div className="spacing-y-0">
        <ColorBlock
          onChange={updateMaxInterval}
          cellColor={maxInterval?.cellColor}
          value={maxInterval?.value}
          disabled={false}
        />
        {colorsBlocks}
        <ColorBlock
          onChange={updateMinInterval}
          cellColor={minInterval?.cellColor}
          value={minInterval?.value}
          disabled={false}
        />
      </div>
    </div>
  );
}
