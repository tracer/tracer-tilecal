type Props = {
  colors: string[];
  onColorClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
};

export default function ColorPalette({ colors, onColorClick }: Props) {
  return (
    <div className="flex">
      {onColorClick &&
        colors.map((color, index) => (
          <button
            onClick={onColorClick}
            key={index}
            value={color}
            className="flex h-3 w-[18px]"
            style={{ backgroundColor: color }}
          ></button>
        ))}
      {!onColorClick &&
        colors.map((color, index) => (
          <span key={index} className="flex h-3 w-[18px]" style={{ backgroundColor: color }}></span>
        ))}
    </div>
  );
}
