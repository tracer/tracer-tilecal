// components/RangeSlider.tsx
import cn from "classnames";

import React from "react";
import ReactSlider, { ReactSliderProps } from "react-slider";

const RangeSlider = <T extends number | readonly number[]>(_props: ReactSliderProps<T>) => {
  const isVertical = _props.orientation === "vertical";
  return (
    <ReactSlider
      {..._props}
      className="h-4"
      renderThumb={(props, state) => (
        <div
          {...props}
          className={cn({
            "h-full": !isVertical,
            "w-full": isVertical,
            "flex aspect-square cursor-grab items-center justify-center rounded-full bg-accent2 text-xs text-white dark:bg-accent1":
              true,
          })}
        >
          {state.valueNow}
        </div>
      )}
      renderTrack={(props, state) => {
        const points = Array.isArray(state.value) ? state.value.length : null;
        const isMulti = points && points > 0;
        const isLast = isMulti ? state.index === points : state.index === 1;
        const isFirst = state.index === 0;
        return (
          <div
            {...props}
            className={cn({
              //use 1/4 height or width depending on the orientation and make sure to center it.
              "top-1/2 h-1/4 -translate-y-1/2": !isVertical,
              "left-1/2 w-1/4 -translate-x-1/2": isVertical,
              "rounded-full": true,
              "bg-gray-200 dark:bg-gray-200": isMulti ? isFirst || isLast : isLast,
              "bg-accent2 dark:bg-accent1 ": isMulti ? !isFirst || !isLast : isFirst,
            })}
          ></div>
        );
      }}
    />
  );
};
export default RangeSlider;
