// components/ReactSliderNonGeneric.tsx
import React from "react";
import ReactSlider, { ReactSliderProps } from "react-slider";

const RangeSliderNonGeneric = (_props: ReactSliderProps) => {
  return <ReactSlider {..._props} className="h-full w-full" />;
};
export default RangeSliderNonGeneric;
