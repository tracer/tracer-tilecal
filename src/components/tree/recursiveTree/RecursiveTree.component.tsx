import { memo } from "react";

import type { TreeNode } from "#/types/app.types";

import ChildNode from "../childNode/ChildNode.component";
import ParentNode from "../parentNode/ParentNode.component";

type Props = {
  tree: TreeNode[];
};

const RecursiveTree = memo(
  function RecursiveT({ tree }: Props) {
    if (!tree) {
      return <></>;
    }

    const elements = tree.map((node: TreeNode) => {
      const { id, name, state, showChildren, root, partition, nodeEnd } = node;
      if (!node.children) {
        return (
          <ChildNode
            key={id}
            uid={id}
            name={name}
            modelState={state}
            nodeEnd={nodeEnd}
            partition={partition}
          />
        );
      } else {
        const show = showChildren ? (showChildren === true ? true : false) : false;
        return (
          <ParentNode
            key={id}
            uid={id}
            name={name}
            modelState={state}
            showChildren={show}
            root={root}
            nodeEnd={nodeEnd}
            partition={partition}
          >
            <RecursiveTree tree={node.children as TreeNode[]} />
          </ParentNode>
        );
      }
    });

    return <>{elements}</>;
  },
  (prevProps, nextProps) => {
    return prevProps.tree === nextProps.tree;
  }
);

export default RecursiveTree;
