import { useEffect, useMemo } from "react";

import { selectModelsLoadingState, updateModelsLoadingState } from "#/store/features/modelSlice";
import {
  selectDetailedTree,
  selectGeometryMenu,
  selectGeometryTree,
  setDetailedTree,
} from "#/store/features/treeSlice";
import { useAppDispatch, useAppSelector } from "#/store/hooks";
// import RecursiveTree from "../recursiveTree/RecursiveTree";
import RecursiveTree from "#/components/tree/recursiveTree/RecursiveTree.component";
import { Checkbox } from "#/components/ui/checkbox";
import useLoadingStatus from "#/hooks/useLoading";

export default function Tree() {
  const dispatch = useAppDispatch();
  const { isLoading, isLoaded } = useLoadingStatus();
  const loadedStatus = useAppSelector(selectModelsLoadingState);
  const geometryTree = useAppSelector(selectGeometryTree);
  const showGeometryMenu = useAppSelector(selectGeometryMenu);
  const detailedTree = useAppSelector(selectDetailedTree);

  const handleDetailedTree = (): void => {
    dispatch(setDetailedTree(!detailedTree));
  };

  useEffect(() => {
    if (isLoading) {
      dispatch(updateModelsLoadingState("loading"));
    } else if (isLoaded) {
      dispatch(updateModelsLoadingState("idle"));
    }
  }, [isLoading, isLoaded, dispatch, detailedTree]);

  const disablePointerEvents = loadedStatus === "loading" ? "pointer-events-none" : null;

  const GeometriesTree = useMemo(() => {
    return <RecursiveTree tree={geometryTree} />;
  }, [geometryTree]);

  return (
    <div
      id="geometry-tree"
      className="absolute top-20 z-20 max-h-[80%]  overflow-y-auto bg-transparent"
    >
      {showGeometryMenu && (
        <>
          <div className="flex w-auto items-center  gap-2 p-2 text-graywhite">
            <label htmlFor="treeCheckBox" className=" pl-2 text-sm font-thin">
              Detailed Tree
            </label>
            <Checkbox
              style={{ backgroundColor: "transparent", color: "white" }}
              onCheckedChange={handleDetailedTree}
              checked={detailedTree}
              id="treeCheckBox"
            />
          </div>
          <ul className={`select-none ${disablePointerEvents}`}>{GeometriesTree}</ul>
        </>
      )}
    </div>
  );
}
