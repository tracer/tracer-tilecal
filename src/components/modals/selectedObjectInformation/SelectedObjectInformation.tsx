import { CellData, ModelInfo, ModuleData } from "#/types/app.types";
import { uid } from "#/utils/uid";
import {
  selectSelectedObject,
  selectSelectionModal,
  setSelectionModal,
} from "#/store/features/selection";
import { useAppDispatch, useAppSelector } from "#/store/hooks";
import Modal from "#/components/modals/Modal.component";

type Key = keyof ModuleData | keyof CellData | keyof ModelInfo;

export default function SelectedObjectInformation() {
  const dispatch = useAppDispatch();
  const show = useAppSelector(selectSelectionModal);
  const selectedObject = useAppSelector(selectSelectedObject);
  let title = "";

  if (selectedObject) {
    if (!("object" in selectedObject)) {
      return null;
    } else {
      title = selectedObject?.object;
    }
  }

  const propsHtml =
    selectedObject &&
    Object.entries(selectedObject).map(([k, value]) => {
      const key = k as Key;
      if (
        value != undefined &&
        [
          "sectionName",
          "module",
          "name",
          "phi",
          "eta",
          "energy",
          "calibration constants",
          "noiseConstant",
          "channels",
          "pmts",
          "pmt1Energy",
          "pmt2Energy",
          "pmt1Time",
          "pmt2Time",
          "radius",
          "cellType",
          "towers",
        ].includes(key)
      ) {
        return (
          <div key={uid()} className="relative grid grid-cols-[auto,120px] items-center gap-[7px]">
            <label htmlFor="name">
              {key === "noiseConstant" && Array.isArray(value) ? value[0] : key}
            </label>
            <input
              id="Name"
              readOnly
              value={
                key === "noiseConstant" && Array.isArray(value)
                  ? value[1].toString().replace(",", ",  ")
                  : value.toString().replace(",", ",  ")
              }
              className="rounded bg-gray2 px-1 py-1"
              onKeyDown={(e) => e.stopPropagation()}
            />
          </div>
        );
      } else return null;
    });

  return (
    <Modal
      id="selectionModal"
      show={show}
      onCloseHandler={(): void => {
        dispatch(setSelectionModal(false));
      }}
      title={title}
    >
      <div className="flex w-full flex-col gap-[7px] px-1 pt-1 text-xs">{propsHtml}</div>
    </Modal>
  );
}
