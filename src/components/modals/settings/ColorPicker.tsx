import { useEffect, useRef } from "react";
import { HexColorPicker } from "react-colorful";

import { selectColorSchemes, setCustomColorScheme } from "#/store/features/tcalSlice";
import { useAppDispatch, useAppSelector } from "#/store/hooks";

export default function ColorPicker({ color }: { color: string }) {
  const dispatch = useAppDispatch();
  const colorSchemes = useAppSelector(selectColorSchemes);
  const selectedColIndex = useRef(0);
  useEffect(() => {
    selectedColIndex.current = colorSchemes.customScheme.findIndex(
      (schemeCol) => schemeCol === color
    );
  }, [color]);

  const handleOnChange = (col: string) => {
    const scheme = colorSchemes.customScheme.map((schemeCol, index) => {
      return selectedColIndex.current === index ? col : schemeCol;
    });

    dispatch(setCustomColorScheme(scheme));
  };

  return <HexColorPicker className="mt-16 px-4 py-8" color={color} onChange={handleOnChange} />;
}
