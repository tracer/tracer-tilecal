import { useState } from "react";

import { ColorSchemes } from "#/types/app.types";
import {
  selectAmbientLightIntensity,
  selectDirectionalLightIntensity,
  selectRotationSpeed,
  setAmbientLightIntensity,
  setControlRotationSpeed,
  setDirectionalLightIntensity,
} from "#/store/features/cameraSlice";
import {
  selectAxis,
  selectGrid,
  selectStats,
  setUtilsModal,
  showAxis,
  showGrid,
  showRendererStats,
} from "#/store/features/globalsSlice";
import {
  selectColorSchemes,
  selectEnergyColorScheme,
  setEnergyColorScheme,
} from "#/store/features/tcalSlice";
import { useAppDispatch, useAppSelector } from "#/store/hooks";
import Button from "#/components/Button.component";
import ColorPalette from "#/components/ColorPalette";
import Modal from "#/components/modals/Modal.component";
import ColorPicker from "#/components/modals/settings/ColorPicker";
import SettingCheckBox from "#/components/modals/settings/SettingsCheckbox";
import SettingsSlider from "#/components/modals/settings/SettingsSlider";
import { Label } from "#/components/ui/label";
import { RadioGroup, RadioGroupItem } from "#/components/ui/radio-group";
import useEscapeKeydown from "#/hooks/useEscapeKeydown";

type SelectedColor = string | undefined;

export default function SettingsMod() {
  const dispatch = useAppDispatch();
  const rotationSpeed = useAppSelector(selectRotationSpeed);
  const ambientLightIntensity = useAppSelector(selectAmbientLightIntensity);
  const directionalLightIntensity = useAppSelector(selectDirectionalLightIntensity);
  const statsIsActive = useAppSelector(selectStats);
  const axisIsActive = useAppSelector(selectAxis);
  const gridIsActive = useAppSelector(selectGrid);
  const colorSchemes = useAppSelector(selectColorSchemes);
  const energyColorScheme = useAppSelector(selectEnergyColorScheme);

  const [selectedColor, setSelectedColor] = useState<SelectedColor>(undefined);
  useEscapeKeydown(() => {
    dispatch(setUtilsModal(false));
  });

  const handRotationSpeedupdate = (e: number | number[]): void => {
    if (typeof e !== "object") {
      dispatch(setControlRotationSpeed(e));
    }
  };

  const handleBrightnessUpdate = (e: number | number[]): void => {
    if (typeof e !== "object") {
      dispatch(setAmbientLightIntensity(e));
    }
  };

  const handleContrastUpdate = (e: number | number[]): void => {
    if (typeof e !== "object") {
      dispatch(setDirectionalLightIntensity(e));
    }
  };

  const handleAxisToggle = (): void => {
    dispatch(showAxis(!axisIsActive));
  };

  const handleGridToggle = (): void => {
    dispatch(showGrid(!gridIsActive));
  };

  const handleStatsToggle = (): void => {
    dispatch(showRendererStats(!statsIsActive));
  };

  const handleCloseModal = (): void => {
    dispatch(setUtilsModal(false));
  };

  const handleSchemeChange = (value: keyof ColorSchemes): void => {
    dispatch(setEnergyColorScheme(value));
  };

  const handleColorClick = (event: React.MouseEvent<HTMLButtonElement>): void => {
    const target = event.target as HTMLButtonElement;
    setSelectedColor(target.value);
  };

  const handlePickerSave = (): void => {
    setSelectedColor(undefined);
  };

  const sliderElements = [
    {
      title: "Camera rotation speed",
      step: 0.01,
      min: 0.1,
      max: 3,
      defaultValue: rotationSpeed,
      value: rotationSpeed,
      onChange: handRotationSpeedupdate,
    },
    {
      title: "Brightness",
      step: 0.001,
      min: 0.1,
      max: 1,
      defaultValue: ambientLightIntensity,
      value: ambientLightIntensity,
      onChange: handleBrightnessUpdate,
    },
    {
      title: "Contrast",
      step: 0.001,
      min: 0.1,
      max: 1,
      defaultValue: directionalLightIntensity,
      value: directionalLightIntensity,
      onChange: handleContrastUpdate,
    },
  ];

  const checkboxElements = [
    {
      title: "Axis",
      id: "axis",
      checked: axisIsActive,
      onClick: handleAxisToggle,
    },
    {
      title: "Grid",
      id: "grid",
      checked: gridIsActive,
      onClick: handleGridToggle,
    },
    {
      title: "Stats",
      id: "stats",
      checked: statsIsActive,
      onClick: handleStatsToggle,
    },
  ];

  const sliderHtml = sliderElements.map((element, idx) => (
    <SettingsSlider key={idx + element.title} {...element} />
  ));

  const checkboxHtml = checkboxElements.map((element, idx) => (
    <SettingCheckBox key={element.title + idx} {...element} />
  ));

  return (
    <Modal id="settings" title="Settings" show={true} onCloseHandler={handleCloseModal}>
      <div className="mt-2 flex items-center justify-between px-2 text-xs">{checkboxHtml}</div>
      <div className="mt-3 text-xs">{sliderHtml}</div>
      <div className="flex">
        <div className="mt-4">
          <h4 className="mb-3">Color Schemes</h4>
          <RadioGroup
            onValueChange={handleSchemeChange}
            className="mt-1"
            defaultValue={energyColorScheme}
          >
            <span className="text-xs">Default scheme</span>
            <div className="flex items-center space-x-2">
              <RadioGroupItem value="default" id="r1" />
              <Label htmlFor="r1">
                <ColorPalette colors={colorSchemes.default} />
              </Label>
            </div>
            <span className="text-xs">Colorblind scheme</span>
            <div className="flex items-center space-x-2">
              <RadioGroupItem value="colorblind" id="r3" />
              <Label htmlFor="r3">
                <ColorPalette colors={colorSchemes.colorblind} />
              </Label>
            </div>
            <span className="text-xs">Low exposure scheme</span>
            <div className="flex items-center space-x-2">
              <RadioGroupItem value="lowExposure" id="r2" />
              <Label htmlFor="r2">
                <ColorPalette colors={colorSchemes.lowExposure} />
              </Label>
            </div>
            <span className="text-xs">Choose Custom Scheme</span>
            <div className="flex items-center space-x-2">
              <RadioGroupItem value="customScheme" id="r4" />
              <Label htmlFor="r4">
                <ColorPalette onColorClick={handleColorClick} colors={colorSchemes.customScheme} />
              </Label>
              {selectedColor && energyColorScheme === "customScheme" && (
                <Button onClick={handlePickerSave} className="top-0 p-1 text-xs">
                  Hide
                </Button>
              )}
            </div>
          </RadioGroup>
        </div>
        {selectedColor && energyColorScheme === "customScheme" && (
          <ColorPicker color={selectedColor} />
        )}
      </div>
    </Modal>
  );
}
