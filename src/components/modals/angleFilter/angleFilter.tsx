import React from "react";

import { selectAngleFilterModal, setAngleFilterModal } from "#/store/features/globalsSlice";
import {
  selectEtaAngleFilterLimits,
  selectPhiAngleFilterLimits,
  selectThetaAngleFilterLimits,
  setAngleFilterEtaLimits,
  setAngleFilterPhiLimits,
  setAngleFilterThetaLimits,
} from "#/store/features/tcalSlice";
import { useAppDispatch, useAppSelector } from "#/store/hooks";
import Modal from "#/components/modals/Modal.component";
import RangeSlider from "#/components/rangeSlider";

export default function AngleFilterModal() {
  const dispatch = useAppDispatch();
  const thetaValues = useAppSelector(selectThetaAngleFilterLimits);
  const phiValues = useAppSelector(selectPhiAngleFilterLimits);
  const etaValues = useAppSelector(selectEtaAngleFilterLimits);
  const show = useAppSelector(selectAngleFilterModal);

  const handleThetaUpdate = (values: number[]) => {
    dispatch(
      setAngleFilterThetaLimits({
        values: values as [number, number],
        defaultValues: thetaValues.defaultValues,
      })
    );
  };
  const handlePhiUpdate = (values: number[]) => {
    dispatch(
      setAngleFilterPhiLimits({
        values: values as [number, number],
        defaultValues: phiValues.defaultValues,
      })
    );
  };
  const handleEtaUpdate = (values: number[]) => {
    dispatch(
      setAngleFilterEtaLimits({
        values: values as [number, number],
        defaultValues: etaValues.defaultValues,
      })
    );
  };

  return (
    <Modal
      id="angleFilterModal"
      show={show}
      onCloseHandler={(): void => {
        dispatch(setAngleFilterModal(false));
      }}
      title="Angle Filter"
    >
      <div className="flex w-full flex-col gap-[7px] px-1 pt-1 text-xs">
        <div className="flex w-full flex-col gap-2">
          <p className="text-gray-400">Filter by theta</p>
          <RangeSlider
            min={thetaValues.defaultValues[0]}
            max={thetaValues.defaultValues[1]}
            step={0.1}
            className="h-8 w-40"
            defaultValue={thetaValues.values}
            onChange={(values) => handleThetaUpdate(values as unknown as number[])}
          />
        </div>
      </div>

      <div className="flex w-full flex-col gap-[7px] px-1 pt-1 text-xs">
        <div className="flex w-full flex-col gap-2">
          <p className="text-gray-400">Filter by Phi</p>
          <RangeSlider
            min={phiValues.defaultValues[0]}
            max={phiValues.defaultValues[1]}
            step={0.1}
            className="h-8 w-40"
            defaultValue={phiValues.values}
            onChange={(values) => handlePhiUpdate(values as unknown as number[])}
          />
        </div>
      </div>

      <div className="flex w-full flex-col gap-[7px] px-1 pt-1 text-xs">
        <div className="flex w-full flex-col gap-2">
          <p className="text-gray-400">Filter by Eta</p>
          <RangeSlider
            min={etaValues.defaultValues[0]}
            max={etaValues.defaultValues[1]}
            step={0.01}
            className="h-8 w-40"
            defaultValue={etaValues.values}
            onChange={(values) => handleEtaUpdate(values as unknown as number[])}
          />
        </div>
      </div>
    </Modal>
  );
}
