import {
  noisePerCellConstants,
  selectParameterPerChannelMinMax,
  selectSelectedConstant,
  selectSelectedGain,
  setNoisePerCellFileData,
  setSelectedConstant,
  setSelectedGain,
  setUploadedFiles,
} from "#/store/features/tcalSlice";
import { useAppDispatch, useAppSelector } from "#/store/hooks";
import MultipleFilesNavigation from "#/components/MultipleFilesNavigation";

export default function NoisePerCellMode() {
  const dispatch = useAppDispatch();
  const minMaxEnergies = useAppSelector(selectParameterPerChannelMinMax);
  const selectedConstant = useAppSelector(selectSelectedConstant);
  const selectedGain = useAppSelector(selectSelectedGain);
  const maxGain = Object.keys(minMaxEnergies[selectedConstant]).length;

  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    dispatch(setUploadedFiles(event.target.files));
    if (file) {
      const reader = new FileReader();
      reader.onload = function (e) {
        const contents = e.target?.result;
        if (typeof contents === "string") {
          dispatch(setNoisePerCellFileData(contents));
        }
      };
      reader.readAsText(file);
    }
  };

  const handleConstantChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    dispatch(setSelectedConstant(event.target.value));
  };
  const handleGainChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const gain = event.target.value;
    dispatch(setSelectedGain(gain));
  };

  return (
    <div>
      <div>
        <input
          type="file"
          id="fileInput"
          className="rounded-md bg-gray1 p-2 text-xs"
          onChange={handleFileChange}
          multiple
        />
      </div>
      <div>
        <label className="text-sm" htmlFor="modeSelect">
          Select Constant:
        </label>
        <select
          id="modeSelect"
          className="m-3 rounded-md bg-gray1 p-2 text-xs"
          onChange={handleConstantChange}
          value={selectedConstant}
        >
          {noisePerCellConstants.map((constant) => (
            <option key={constant} value={constant}>
              {constant}
            </option>
          ))}
        </select>
      </div>

      <div>
        <label className="text-sm" htmlFor="modeSelect">
          Select Gain:
        </label>
        <select
          onChange={handleGainChange}
          id="modeSelect"
          className="m-3 rounded-md bg-gray1 p-2 text-xs"
          value={selectedGain}
        >
          {Array.from({ length: maxGain }, (_, index) => (
            <option key={index} value={index}>
              {index}
            </option>
          ))}
        </select>
      </div>

      <MultipleFilesNavigation />
    </div>
  );
}
