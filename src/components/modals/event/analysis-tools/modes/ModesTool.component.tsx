import { CellColorMode } from "#/types/app.types";
import { selectPmtInfoExists } from "#/store/features/eventSlice";
import {
  cellColorModes,
  selectCellColorMode,
  selectVisualisationType,
  setCellColorMode,
  setNoisePerCellFileData,
  setParameterPerChannelFileData,
  setSelectedConstant,
} from "#/store/features/tcalSlice";
import { useAppDispatch, useAppSelector } from "#/store/hooks";
import CalibrationConstantsMode from "#/components/modals/event/analysis-tools/modes/CalibrationConstantsMode";
import NoisePerCellMode from "#/components/modals/event/analysis-tools/modes/NoisePerCellMode";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "#/components/ui/select";

export default function ModesTool() {
  const dispatch = useAppDispatch();
  const defaultCellColorMode = useAppSelector(selectCellColorMode);
  const defaultVisualisationType = useAppSelector(selectVisualisationType);
  const pmtInfoExists = useAppSelector(selectPmtInfoExists);
  const cellColorMode = useAppSelector(selectCellColorMode);

  const handleOnChange = async (colorMode: CellColorMode) => {
    if (colorMode === "calibration constants per channel") {
      const response = await fetch("/cell-mode-files/calib-const.txt");
      const data = await response.text();
      dispatch(setParameterPerChannelFileData(data));
      dispatch(setSelectedConstant("0"));
    }
    if (colorMode === "noise per cell") {
      const response = await fetch("/cell-mode-files/noisePerCell.txt");
      const data = await response.text();
      dispatch(setNoisePerCellFileData(data));
      dispatch(setSelectedConstant("RMS"));
    }
    dispatch(setCellColorMode(colorMode));
  };

  // const handleOnTypeChange = async (typeOption: VisualisationType) => {
  //   dispatch(setVisualisationType(typeOption));
  // };

  const modeOptions = cellColorModes.map((mode) => {
    const disalbed =
      (mode === "pmt1/pmt2 energies" && !pmtInfoExists) ||
      (mode === "noise per cell" && defaultVisualisationType === "parameter alongside eta") ||
      (mode === "calibration constants per channel" &&
        defaultVisualisationType === "parameter alongside eta");

    return (
      <SelectItem disabled={disalbed} key={mode} value={mode}>
        {mode}
      </SelectItem>
    );
  });

  // const typeOptions = visualisationTypes.map((type) => {
  //   return (
  //     <SelectItem key={type} value={type}>
  //       {type}
  //     </SelectItem>
  //   );
  // });

  return (
    <div className="flex flex-col gap-2 py-2">
      {/* <span className="pl-1 text-xs text-white">Change visualisation type:</span>
      <Select defaultValue={defaultVisualisationType} onValueChange={handleOnTypeChange}>
        <SelectTrigger className="w-full bg-gray1 text-white">
          <SelectValue />
        </SelectTrigger>
        <SelectContent className="bg-gray1">{typeOptions}</SelectContent>
      </Select> */}
      <span className="pl-1 text-xs text-white">Change cell color parameter:</span>
      <Select defaultValue={defaultCellColorMode} onValueChange={handleOnChange}>
        <SelectTrigger className="w-full bg-gray1 text-white">
          <SelectValue />
        </SelectTrigger>
        <SelectContent className="bg-gray1">{modeOptions}</SelectContent>
      </Select>
      {cellColorMode === "calibration constants per channel" && <CalibrationConstantsMode />}
      {cellColorMode === "noise per cell" && <NoisePerCellMode />}
    </div>
  );
}
