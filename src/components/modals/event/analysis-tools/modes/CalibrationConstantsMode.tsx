import {
  selectParameterPerChannel,
  selectSelectedConstant,
  selectSelectedGain,
  setParameterPerChannelFileData,
  setSelectedConstant,
  setSelectedGain,
  setUploadedFiles,
} from "#/store/features/tcalSlice";
import { useAppDispatch, useAppSelector } from "#/store/hooks";
import MultipleFilesNavigation from "#/components/MultipleFilesNavigation";

export default function CalibrationConstantsMode() {
  const dispatch = useAppDispatch();
  const parameterPerChannel = useAppSelector(selectParameterPerChannel);
  const selectedConstant = useAppSelector(selectSelectedConstant);
  const selectedGain = useAppSelector(selectSelectedGain);
  const secondGainExists = parameterPerChannel["LBA"]["01"]["0"][1].length > 0;

  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    dispatch(setUploadedFiles(event.target.files));
    if (file) {
      const reader = new FileReader();
      reader.onload = function (e) {
        const contents = e.target?.result;
        if (typeof contents === "string") {
          dispatch(setParameterPerChannelFileData(contents));
        }
      };
      reader.readAsText(file);
    }
  };

  const handleConstantChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    dispatch(setSelectedConstant(event.target.value));
  };
  const handleGainChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const gain = event.target.value;
    dispatch(setSelectedGain(gain));
  };

  return (
    <div>
      <input
        type="file"
        id="fileInput"
        className="rounded-md bg-gray1 p-2 text-xs"
        onChange={handleFileChange}
        multiple
      />
      <div>
        <label className="text-sm" htmlFor="modeSelect">
          Select Constant:
        </label>
        <select
          id="modeSelect"
          className="m-3 rounded-md bg-gray1 p-2 text-xs"
          onChange={handleConstantChange}
          value={selectedConstant}
        >
          {Array.from({ length: 7 }, (_, index) => (
            <option key={index} value={index}>
              Constant {index + 1}
            </option>
          ))}
        </select>
      </div>

      <div>
        <label className="text-sm" htmlFor="modeSelect">
          Select Gain:
        </label>
        <select
          onChange={handleGainChange}
          id="modeSelect"
          className="m-3 rounded-md bg-gray1 p-2 text-xs"
          value={selectedGain}
        >
          <option value="0">0</option>
          {secondGainExists && <option value="1">1</option>}
        </select>
      </div>

      <MultipleFilesNavigation />
    </div>
  );
}
