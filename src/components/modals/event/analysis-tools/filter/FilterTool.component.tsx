import {
  selectFilterCellColorsMinMax,
  setFilterCellColorsMinMax,
} from "#/store/features/tcalSlice";
import { useAppDispatch, useAppSelector } from "#/store/hooks";

import FilterContainer from "./FilterContainer.component";
import FilterInput from "./FilterInput.component";

export default function FilterTool() {
  const dispatch = useAppDispatch();
  const filter = useAppSelector(selectFilterCellColorsMinMax);

  const cellColorMinMaxFilter = (key: string, value: string): void => {
    const val = value === "" ? undefined : value;

    dispatch(
      setFilterCellColorsMinMax({
        ...filter,
        [key]: val,
      })
    );
  };

  const onCellColorsFilterReset = (): void => {
    dispatch(
      setFilterCellColorsMinMax({
        min: undefined,
        max: undefined,
      })
    );
  };

  return (
    <div className="flex flex-col justify-between  p-2">
      <div className="flex w-full flex-col gap-2 md:flex-row md:justify-between">
        <FilterContainer
          title="Filter Cells"
          resetButtonTitle="Reset cell filter values"
          onFieldsReset={onCellColorsFilterReset}
        >
          <FilterInput
            filter="min"
            placeholder="Enter min value"
            onChangeHandler={cellColorMinMaxFilter}
            value={filter.min}
          />
          <FilterInput
            filter="max"
            placeholder="Enter max value"
            onChangeHandler={cellColorMinMaxFilter}
            value={filter.max}
          />
        </FilterContainer>
      </div>
    </div>
  );
}
