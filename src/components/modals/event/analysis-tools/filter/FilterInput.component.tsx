import { ChangeEvent } from "react";

interface Props {
  onChangeHandler: (key: string, value: string) => void;
  filter: string;
  value: string | undefined;
  placeholder?: string;
}

const numberPattern = /^-?[0-9]*\.?[0-9]*$/;

export default function FilterInput({ filter, placeholder, value, onChangeHandler }: Props) {
  const handleInputChange = (event: ChangeEvent<HTMLInputElement>): void => {
    const inputValue = event.target.value;
    const isValid = numberPattern.test(inputValue);

    if (isValid) {
      if (inputValue.indexOf(".") === 0) {
        onChangeHandler(filter, "0" + inputValue);
      } else {
        onChangeHandler(filter, inputValue);
      }
    }
  };

  return (
    <div className="relative flex w-full flex-wrap items-center">
      <input
        onBlur={(): void => window.scrollTo(0, 0)}
        placeholder={placeholder}
        className="ml-[-3px] w-full flex-grow  overflow-y-hidden rounded-sm rounded-t-sm border border-dark3 bg-highlight1 p-1 px-[0.25rem] text-base text-accent3 outline-none placeholder:text-gray-600 dark:border-highlight1 dark:bg-gray1 sm:text-base"
        onChange={handleInputChange}
        value={value ? value : ""}
      />
    </div>
  );
}
