import { useMemo } from "react";

import { selectEventDisabledObjects, selectLoadedEvents } from "#/store/features/eventSlice";
import {
  selectCurrentEventAnalysisTool,
  selectEventsModalState,
  showEventsModal,
} from "#/store/features/modalSlice";
import { useAppDispatch, useAppSelector } from "#/store/hooks";
import EventsResultsToggler from "#/components/modals/event/event-objects/EventsResultsToggler.component";
import FileActions from "#/components/modals/event/file-actions/FileActions.component";
import useEscapeKeydown from "#/hooks/useEscapeKeydown";

import Modal from "../Modal.component";
import AnalysisTools from "./analysis-tools/AnalysisTools.component";
import FilterTool from "./analysis-tools/filter/FilterTool.component";
import InfoTool from "./analysis-tools/Info/InfoTool.component";
import ModesTool from "./analysis-tools/modes/ModesTool.component";

export default function EventsModal() {
  const dispatch = useAppDispatch();
  const show = useAppSelector(selectEventsModalState);
  const loadedEvents = useAppSelector(selectLoadedEvents);
  const disabledEventObjects = useAppSelector(selectEventDisabledObjects);
  const currentAnalysisTool = useAppSelector(selectCurrentEventAnalysisTool);

  const memoizedAnalysisTool = useMemo(() => {
    return currentAnalysisTool;
  }, [currentAnalysisTool]);

  useEscapeKeydown(() => dispatch(showEventsModal(false)));

  const closeModalHandler = (): void => {
    dispatch(showEventsModal(false));
  };

  const renderCurrentTool = () => {
    switch (memoizedAnalysisTool) {
      case "info":
        return (
          <div className="max-h-52 overflow-y-auto">
            {loadedEvents.map((event, index) => {
              return (
                <InfoTool
                  active={index === 0}
                  showEventDetails={index === 0}
                  key={index}
                  eventName={event.eventName}
                  num={event.eventNumber}
                  lumiBlocks={event.lumiBlock}
                  runNumber={event.runNumber}
                  date={event.date}
                  time={event.time}
                  loadedEvent={event.loadedEvent}
                  disabledEventObjects={disabledEventObjects}
                />
              );
            })}
            <span className="mr-3 text-right text-xs">Loaded events {loadedEvents.length}</span>
          </div>
        );
      case "modes":
        return <ModesTool />;
      case "filter":
        return <FilterTool />;
      default:
        return <></>;
    }
  };

  const currentTool = renderCurrentTool();

  return (
    <Modal id="events" title="Events" show={show} onCloseHandler={closeModalHandler}>
      <div className="py-2">
        <FileActions />
        <EventsResultsToggler />
        <AnalysisTools />
      </div>
      {currentTool}
    </Modal>
  );
}
