import { DisabledEventObject } from "#/types/app.types";
import {
  selectEventDisabledObjects,
  selectEventParameters,
  setEventParameters,
} from "#/store/features/eventSlice";
import { useAppDispatch, useAppSelector } from "#/store/hooks";

import Checkbox from "./Checkbox.component";

export default function EventsResultsToggler() {
  const dispatch = useAppDispatch();
  const eventParameters = useAppSelector(selectEventParameters);
  const disabledEventObjects = useAppSelector(selectEventDisabledObjects);

  const eventLabels: { key: DisabledEventObject; label: string }[] = [
    { key: "track", label: "tracks" },
    { key: "jet", label: "jets" },
    { key: "etmis", label: "met" },
  ];

  const handleEventToggle = (key: string) => () => {
    dispatch(
      setEventParameters({
        ...eventParameters,
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        [key]: !eventParameters[key],
      })
    );
  };

  return (
    <div className="mt-4 flex justify-between gap-2">
      {eventLabels.map(({ key, label }) => {
        const disabled = disabledEventObjects.includes(key);

        return (
          <div className="flex items-center gap-2" key={key + label}>
            <Checkbox
              id={label}
              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
              // @ts-ignore
              checked={eventParameters[key]}
              onClick={handleEventToggle(key)}
              disabled={disabled}
            />
            <p className="select-none text-xs text-textColor">{label.toUpperCase()}</p>
          </div>
        );
      })}
    </div>
  );
}
