import {
  selectAxis,
  selectGrid,
  selectMenuBar,
  selectStats,
  setMenuBar,
  showAxis,
  showGrid,
  showRendererStats,
} from "#/store/features/globalsSlice";
import {
  selectEventsModalState,
  showEventsModal,
} from "#/store/features/modalSlice";
import {
  selectGeometryMenu,
  setGeometryMenuVisibility,
} from "#/store/features/treeSlice";
import { useAppDispatch, useAppSelector } from "#/store/hooks";
import AboutMenu from "#/components/navigation/AboutMenu";
import AngleFilterMenu from "#/components/navigation/angleFilterMenu";
import CameraViewMenu from "#/components/navigation/CameraViewMenu";
import DroneMenu from "#/components/navigation/DroneMenu";
import EventsMenu from "#/components/navigation/EventsMenu";
import FullScreenMenu from "#/components/navigation/FullScreenMenu";
import GeometryMenu from "#/components/navigation/GeometryMenu";
import NavigationBar from "#/components/navigation/navigationToggler";
import NavTitle from "#/components/navigation/NavTitle";
import Report from "#/components/navigation/Report";
import Settings from "#/components/navigation/Settings";
import ShowMenuBar from "#/components/navigation/ShowMenuBar";
import ThemeToggler from "#/components/navigation/ThemeToggler";
import Tools from "#/components/navigation/Tools";
import useKeydown from "#/hooks/useKeyDown";

export default function Navigation() {
  const dispatch = useAppDispatch();
  const navigationIsActive = useAppSelector(selectMenuBar);
  const gridIsActive = useAppSelector(selectGrid);
  const statsIsActive = useAppSelector(selectStats);
  const axisIsActive = useAppSelector(selectAxis);
  const eventWindowIsActive = useAppSelector(selectEventsModalState);
  const geometryMenuIsActive = useAppSelector(selectGeometryMenu);

  // navigation
  useKeydown("n", () => {
    dispatch(setMenuBar(!navigationIsActive));
  });

  // stats
  useKeydown("s", () => {
    dispatch(showRendererStats(!statsIsActive));
  });

  // grid
  useKeydown("g", () => {
    gridIsActive ? dispatch(showGrid(false)) : dispatch(showGrid(true));
  });

  // axis
  useKeydown("a", () => {
    axisIsActive ? dispatch(showAxis(false)) : dispatch(showAxis(true));
  });

  // tree
  useKeydown("t", () => {
    dispatch(setGeometryMenuVisibility(!geometryMenuIsActive));
  });

  // events
  useKeydown("e", () => {
    eventWindowIsActive
      ? dispatch(showEventsModal(false))
      : dispatch(showEventsModal(true));
  });

  return (
    <>
      {navigationIsActive && (
        <header className="fixed bottom-0 left-1/2 z-50 flex flex-auto -translate-x-1/2 transform  select-none flex-col items-center justify-center sm:bottom-auto sm:top-0">
          <nav className=" flex  w-full  items-center justify-center gap-2 rounded-sm  bg-dark1 py-2 pl-4 pr-4 sm:w-auto">
            <NavTitle />
            <NavigationBar />
            <GeometryMenu />
            <EventsMenu />
            <DroneMenu />
            <CameraViewMenu />
            <AngleFilterMenu />
            <Tools />
            <Settings />
            <Report />
            <div className="hidden items-center gap-2 sm:flex">
              <ThemeToggler />
              <FullScreenMenu />
              <AboutMenu />
            </div>
          </nav>
        </header>
      )}
      <ShowMenuBar />
    </>
  );
}
