import React from "react";

import Icons from "#/utils/icons";
import { selectAngleFilterModal, setAngleFilterModal } from "#/store/features/globalsSlice";
import { useAppDispatch, useAppSelector } from "#/store/hooks";
import NavIcon from "#/components/navigation/NavIcon";

export default function AngleFilterMenu() {
  const show = useAppSelector(selectAngleFilterModal);
  const dispatch = useAppDispatch();
  return (
    <>
      <NavIcon
        active={show}
        className="fill-graywhite"
        onClick={() => {
          dispatch(setAngleFilterModal(!show));
        }}
        Icon={Icons.CompassDraftingIcon}
        title="Angle Filter"
      />
    </>
  );
}
