export default function NavTitle() {
  return (
    <h1 id="nav-title" className="hidden text-base text-textColor sm:block">
      <span className="text-accent2 dark:text-accent1">T</span>Cal
    </h1>
  );
}
