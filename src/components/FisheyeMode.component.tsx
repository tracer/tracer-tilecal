import Icons from "#/utils/icons";
import { selectFisheye, setFisheye } from "#/store/features/globalsSlice";
import { useAppDispatch, useAppSelector } from "#/store/hooks";
import NavIcon from "#/components/navigation/NavIcon";

export default function FisheyeMode() {
  const dispatch = useAppDispatch();
  const fisheyeMode = useAppSelector(selectFisheye);

  const handleOnClick = () => {
    dispatch(setFisheye(!fisheyeMode));
  };
  return (
    <div className="absolute right-0 top-1 z-20 mr-2 space-y-3">
      <NavIcon Icon={Icons.FisheyeIcon} onClick={handleOnClick} />
    </div>
  );
}
