import { selectModelScale, setModelScale } from "#/store/features/modelSlice";
import { useAppDispatch, useAppSelector } from "#/store/hooks";
import { Slider } from "#/components/ui/slider";

export default function ScaleSlider() {
  const dispatch = useAppDispatch();
  const modelScale = useAppSelector(selectModelScale);

  const handleValueChange = (value: number[]) => {
    dispatch(setModelScale(value[0]));
  };
  return (
    <Slider
      className="absolute bottom-40 left-4 z-10 w-32"
      defaultValue={[modelScale]}
      max={4}
      min={0.1}
      step={0.1}
      onValueChange={handleValueChange}
    />
  );
}
