import {
  selectCellColorMode,
  selectSelectedUploadedFile,
  selectUploadedFiles,
  setNoisePerCellFileData,
  setParameterPerChannelFileData,
  setSelectedUploadedFile,
} from "#/store/features/tcalSlice";
import { useAppDispatch, useAppSelector } from "#/store/hooks";
import Button from "#/components/Button.component";

export default function MultipleFilesNavigation() {
  const dispatch = useAppDispatch();
  const uploadedFiles = useAppSelector(selectUploadedFiles);
  const uploadedFile = useAppSelector(selectSelectedUploadedFile);
  const cellColorMode = useAppSelector(selectCellColorMode);

  const handleFile = (newUploadedFile: number) => {
    dispatch(setSelectedUploadedFile(newUploadedFile));
    const reader = new FileReader();
    reader.onload = function (e) {
      const contents = e.target?.result;
      if (typeof contents === "string") {
        if (cellColorMode === "calibration constants per channel")
          dispatch(setParameterPerChannelFileData(contents));
        if (cellColorMode === "noise per cell") dispatch(setNoisePerCellFileData(contents));
      }
    };
    if (uploadedFiles) reader.readAsText(uploadedFiles[newUploadedFile]);
  };

  return (
    <div className="flex justify-between text-sm">
      <span>{uploadedFiles ? uploadedFiles[uploadedFile]?.name : ""}</span>
      <div>
        <Button
          className="inline-flex h-8 w-8 items-center justify-center"
          onClick={() => {
            const length = uploadedFiles ? uploadedFiles?.length : 0;
            const newUploadedFile = (uploadedFile - 1 + length) % length;

            handleFile(newUploadedFile);
          }}
        >
          &lt;
        </Button>
        <Button
          className="inline-flex h-8 w-8 items-center justify-center"
          onClick={() => {
            const length = uploadedFiles ? uploadedFiles?.length : 0;
            const newUploadedFile = (uploadedFile + 1) % length;

            handleFile(newUploadedFile);
          }}
        >
          &gt;
        </Button>
      </div>
    </div>
  );
}
