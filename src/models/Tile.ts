import Channels from "#/models/Channels";
import Event from "#/models/Event";
import {
  BufferGeometry,
  Color,
  Float32BufferAttribute,
  Mesh,
  ShaderMaterial,
  Vector3,
} from "three";

import type {
  ActiveCell,
  CellColorMode,
  CellData,
  CellProps,
  CellPropsData,
  ParameterPerCellObject,
  ParameterPerChannelMinMax,
  ParameterPerChannelObject,
  Tile,
  VisualisationType,
} from "#/types/app.types";
import { fragmentShader, vertexShader } from "#/lib/shaders/shader";
import { uid } from "#/utils/uid";
import { channelsMap, pmtsMap, towersMap } from "#/constants/Channels";
import { cellProps, faceCoordinatesHelper } from "#/constants/event";
import {
  getCellGeometryVertices,
  getLeftPmtGeometryVertices,
  getRightPmtGeometryVertices,
} from "#/constants/vertices";
import store from "#/store/store";

type TileComponent = {
  data: number[];
  binaryIdRange: [number, number];
};
type CellGeometryType = "default" | "middle" | "top" | "bottom";

export type MeshType = Mesh<BufferGeometry, ShaderMaterial>;

export type TileInfo = {
  fisheye: boolean;
  count: number;
  name: string[];
  pmt1Energy: number[];
  pmt2Energy: number[];
  pmt1Time: number[];
  pmt2Time: number[];
  energy: number[];
  maxEnergy: number;
  minEnergy: number;
  geometries: MeshType[];
  eta: number[];
  phi: number[];
  id: number[];
  centerCoords: Vector3[];
  theta: number[];
  cellProps: CellProps;
  faceCoordinatesHelper: (number[] | number[][])[][];
  channels: number[][];
  pmt: number[][];
  cellType: CellGeometryType[];
  parametersPerTower: Record<string, number>[];
  tileComponents: {
    section: TileComponent;
    module: TileComponent;
    tower: TileComponent;
    sampling: TileComponent;
  };
  sectionName: string[];
};

export default class TileModel extends Event {
  private Channels: Channels;
  energyColorScheme: string[] = [];
  cellColorMode: CellColorMode = "total energies";
  visualisationType: VisualisationType = "default";
  parameterPerChannel: ParameterPerChannelObject = {};
  parameterPerCell: ParameterPerCellObject = {};
  parameterPerChannelMinMax: ParameterPerChannelMinMax | null = null;
  selectedConstant = "0";
  selectedGain = "0";
  radius: number[] = [];
  tileInfo: TileInfo = {
    fisheye: false,
    count: 0,
    geometries: [],
    name: [],
    pmt1Energy: [],
    pmt2Energy: [],
    pmt1Time: [],
    pmt2Time: [],
    energy: [],
    maxEnergy: 0,
    minEnergy: 0,
    eta: [],
    phi: [],
    id: [],
    centerCoords: [],
    theta: [],
    cellProps,
    faceCoordinatesHelper,
    channels: [],
    pmt: [],
    cellType: [],
    parametersPerTower: Array.from({ length: 64 }, () =>
      Object.fromEntries(Array.from({ length: 16 }, (_, i) => [`${i + 1}`, 0]))
    ),
    tileComponents: {
      section: {
        data: [],
        binaryIdRange: [2, 5],
      },
      module: {
        data: [],
        binaryIdRange: [8, 17],
      },
      tower: {
        data: [],
        binaryIdRange: [17, 23],
      },
      sampling: {
        data: [],
        binaryIdRange: [23, 27],
      },
    },
    sectionName: [],
  };

  constructor(ChannelsClass: typeof Channels) {
    super();
    this.Channels = new ChannelsClass();
  }

  reset(): void {
    this.tileInfo = {
      fisheye: false,
      count: 0,
      geometries: [],
      name: [],
      pmt1Energy: [],
      pmt2Energy: [],
      pmt1Time: [],
      pmt2Time: [],
      energy: [],
      maxEnergy: 0,
      minEnergy: 0,
      eta: [],
      phi: [],
      id: [],
      centerCoords: [],
      theta: [],
      cellProps,
      faceCoordinatesHelper,
      channels: [],
      pmt: [],
      cellType: [],
      parametersPerTower: Array.from({ length: 64 }, () =>
        Object.fromEntries(Array.from({ length: 16 }, (_, i) => [`${i + 1}`, 0]))
      ),
      tileComponents: {
        section: {
          data: [],
          binaryIdRange: [2, 5],
        },
        module: {
          data: [],
          binaryIdRange: [8, 17],
        },
        tower: {
          data: [],
          binaryIdRange: [17, 23],
        },
        sampling: {
          data: [],
          binaryIdRange: [23, 27],
        },
      },
      sectionName: [],
    };
  }

  drawCells(activeCells: ActiveCell[]): this {
    activeCells.forEach((activeCell) => this.drawLayer(activeCell));

    return this;
  }

  getGeometries(): MeshType[] {
    return this.tileInfo.geometries;
  }

  init(
    tile: Tile,
    paramPerChannelInfo: {
      parameterPerChannel: ParameterPerChannelObject;
      parameterPerChannelMinMax: ParameterPerChannelMinMax;
    } | null = null
  ): this {
    this.tileInfo.pmt1Energy = tile.pmt1Energy ? this.getNumbersArrayFromTag(tile.pmt1Energy) : [];
    this.tileInfo.pmt2Energy = tile.pmt2Energy ? this.getNumbersArrayFromTag(tile.pmt2Energy) : [];
    this.tileInfo.pmt1Time = tile.pmt1Time ? this.getNumbersArrayFromTag(tile.pmt1Time) : [];
    this.tileInfo.pmt2Time = tile.pmt2Time ? this.getNumbersArrayFromTag(tile.pmt2Time) : [];

    this.tileInfo.count = +tile["@_count"];

    this.tileInfo.energy = this.getNumbersArrayFromTag(tile.energy);
    this.tileInfo.eta = this.getNumbersArrayFromTag(tile.eta);
    this.tileInfo.phi = this.getNumbersArrayFromTag(tile.phi);
    this.tileInfo.id = this.getNumbersArrayFromTag(tile.id);

    this.processBinaryData();
    this.calculateCellWidths();
    this.setZCoordinates();
    this.setNameAndThetaValues();
    this.setChannelsToAllCells();

    const state = store.getState();
    this.tileInfo.fisheye = state.globals.fisheye;

    if (paramPerChannelInfo) {
      this.parameterPerChannel = paramPerChannelInfo.parameterPerChannel;

      this.parameterPerChannelMinMax = paramPerChannelInfo.parameterPerChannelMinMax;
    }

    switch (this.visualisationType) {
      case "parameter alongside eta":
        this.setParametersAlongsideEta();
        return this;
    }
    return this;
  }

  private getTowerNumbers(cellIndex: number): string[] {
    const { sectionName, name } = this.tileInfo;
    const sectionKey = Object.keys(towersMap).find((section) =>
      section.includes(sectionName[cellIndex].slice(1))
    );
    if (!sectionKey || !(sectionKey in towersMap)) {
      throw new Error("Invalid section name");
    }
    const towerNumbers: string[] = [];
    if (name[cellIndex].includes("E")) {
      towerNumbers.push(
        Object.keys(towersMap[sectionKey]).find((tower) =>
          towersMap[sectionKey][tower]["subCells"].includes(name[cellIndex].replace("-", ""))
        ) || ""
      );
    } else {
      towerNumbers.push(
        Object.keys(towersMap[sectionKey]).find((tower) =>
          towersMap[sectionKey][tower]["subCells"].includes(name[cellIndex].replace("-", "") + "R")
        ) || ""
      );
      towerNumbers.push(
        Object.keys(towersMap[sectionKey]).find((tower) =>
          towersMap[sectionKey][tower]["subCells"].includes(name[cellIndex].replace("-", "") + "L")
        ) || ""
      );
    }
    return towerNumbers;
  }

  private setParametersAlongsideEta(): void {
    const { energy, sectionName, parametersPerTower, pmt1Energy, pmt2Energy } = this.tileInfo;
    const { module } = this.tileInfo.tileComponents;
    sectionName.forEach((section, index) => {
      const towerNumbers = this.getTowerNumbers(index);
      if (towerNumbers.length === 0 || towerNumbers[0] === "") {
        throw new Error("Invalid tower number");
      } else if (
        towerNumbers.length === 1 ||
        towerNumbers[0] === towerNumbers[1] ||
        towerNumbers[1] === ""
      ) {
        switch (this.cellColorMode) {
          case "total energies":
            parametersPerTower[module.data[index] - 1][towerNumbers[0]] += energy[index];
            break;
          case "pmt1/pmt2 energies":
            parametersPerTower[module.data[index] - 1][towerNumbers[0]] +=
              pmt1Energy[index] + pmt2Energy[index];
            break;
          case "calibration constants per channel": {
            const calibConstants = this.getCalibConstantsWithCellIndex(index);
            const constants =
              calibConstants[1] !== undefined
                ? calibConstants[0] + calibConstants[1]
                : calibConstants[0];
            parametersPerTower[module.data[index] - 1][towerNumbers[0]] += constants;
            break;
          }
          default:
            throw new Error("Invalid color mode");
        }
      } else {
        switch (this.cellColorMode) {
          case "total energies":
            parametersPerTower[module.data[index] - 1][towerNumbers[0]] += energy[index];
            parametersPerTower[module.data[index] - 1][towerNumbers[1]] += energy[index];
            break;
          case "pmt1/pmt2 energies":
            parametersPerTower[module.data[index] - 1][towerNumbers[0]] += pmt1Energy[index];
            parametersPerTower[module.data[index] - 1][towerNumbers[1]] += pmt2Energy[index];
            break;
          case "calibration constants per channel": {
            const calibConstants = this.getCalibConstantsWithCellIndex(index);
            parametersPerTower[module.data[index] - 1][towerNumbers[0]] += calibConstants[0];
            if (calibConstants[1] !== undefined) {
              parametersPerTower[module.data[index] - 1][towerNumbers[1]] += calibConstants[1];
            } else {
              throw new Error("something went wront when setting energies alongside eta");
            }
            break;
          }
          default:
            throw new Error("Invalid color mode");
        }
      }
    });
  }

  setMinMaxEnergy(
    options: undefined | { min: number; max: number } = undefined,
    fixScale = false,
    useOptions = true
  ): this {
    if (options && (useOptions || fixScale)) {
      this.tileInfo.maxEnergy = options.max;
      this.tileInfo.minEnergy = options.min;
    } else if (this.visualisationType === "parameter alongside eta") {
      const energies = this.tileInfo.parametersPerTower.reduce<number[]>(
        (acc, obj) => acc.concat(Object.values(obj).filter((val) => val)),
        []
      );
      this.tileInfo.maxEnergy = Math.max(...energies);
      this.tileInfo.minEnergy = Math.min(...energies);
    } else {
      switch (this.cellColorMode) {
        case "total energies":
          this.tileInfo.maxEnergy = Math.max(...this.tileInfo.energy);
          this.tileInfo.minEnergy = Math.min(...this.tileInfo.energy);
          break;
        case "pmt1/pmt2 energies":
          this.tileInfo.maxEnergy = Math.max(
            ...this.tileInfo.pmt1Energy,
            ...this.tileInfo.pmt2Energy
          );
          this.tileInfo.minEnergy = Math.min(
            ...this.tileInfo.pmt1Energy,
            ...this.tileInfo.pmt2Energy
          );
          break;
        case "calibration constants per channel": {
          if (this.parameterPerChannelMinMax === null) {
            throw new Error("Parameter per channel invalid data");
          } else {
            this.tileInfo.maxEnergy =
              this.parameterPerChannelMinMax[this.selectedGain][this.selectedConstant]["max"];
            this.tileInfo.minEnergy =
              this.parameterPerChannelMinMax[this.selectedGain][this.selectedConstant]["min"];
          }
          break;
        }
        case "noise per cell": {
          if (this.parameterPerChannelMinMax === null) {
            throw new Error("Parameter per channel invalid data");
          } else {
            this.tileInfo.maxEnergy =
              this.parameterPerChannelMinMax[this.selectedConstant][this.selectedGain]["max"];
            this.tileInfo.minEnergy =
              this.parameterPerChannelMinMax[this.selectedConstant][this.selectedGain]["min"];
          }
          break;
        }
        default:
          this.tileInfo.maxEnergy = Math.max(...this.tileInfo.energy);
          this.tileInfo.minEnergy = Math.min(...this.tileInfo.energy);
      }
    }

    return this;
  }

  private calculateCellWidths(): void {
    const cellProps = this.tileInfo.cellProps;
    const cellNames = ["A", "BLB", "C", "DLBD4", "BEB", "DEB", "E1", "E2", "E3", "E4"];

    for (const cellName of cellNames) {
      cellProps[cellName as keyof CellProps].w = this.calcWidth(
        cellProps[cellName as keyof CellProps]
      );
    }
  }

  private convertToBinary(cellIds: number[]): string[] {
    const { count } = this.tileInfo;
    const idBinary = [];

    for (let i = 0; i < count; i++) {
      idBinary.push(cellIds[i].toString(2));
    }

    return idBinary;
  }

  private extractAndConvertBinarySlice(binary: string, start: number, end: number): number {
    return parseInt(binary.slice(start, end), 2);
  }

  private calcWidth(cellType: CellPropsData): [number, number] {
    const topWidth = 2 * (cellType.r - cellType.h / 2) * Math.atan(Math.PI / 64) - 0.002;
    const bottomWidth = 2 * (cellType.r + cellType.h / 2) * Math.atan(Math.PI / 64) - 0.002;
    return [topWidth, bottomWidth];
  }

  private processBinaryData(): void {
    const idBinary = this.convertToBinary(this.tileInfo.id);
    const { section, module, tower, sampling } = this.tileInfo.tileComponents;

    for (let i = 0; i < this.tileInfo.count; i++) {
      const binaryId = idBinary[i];

      section.data.push(this.extractAndConvertBinarySlice(binaryId, ...section.binaryIdRange));

      module.data.push(
        this.extractAndConvertBinarySlice(binaryId, ...module.binaryIdRange) *
          Math.sign(this.tileInfo.phi[i])
      );

      tower.data.push(this.extractAndConvertBinarySlice(binaryId, ...tower.binaryIdRange));

      sampling.data.push(this.extractAndConvertBinarySlice(binaryId, ...sampling.binaryIdRange));
    }
  }

  private findZCoordinatesForA(): void {
    const { A } = this.tileInfo.cellProps;
    let lengthSum = 0;

    for (let i = 0; i < A.d.length; i++) {
      if (i === 10) lengthSum = 3.5745;
      A.z.push(lengthSum + A.d[i] / 2);
      lengthSum += A.z && A.d[i];
    }
  }

  private findZCoordinatesForB(): void {
    const { B } = this.tileInfo.cellProps;
    let lengthSum = 0;

    for (let i = 0; i < B.d.length; i++) {
      if (i === 9) lengthSum = 3.5745;
      B.z.push(lengthSum + B.d[i] / 2);
      lengthSum += B.z && B.d[i];
    }
  }

  private findZCoordinatesForC(): void {
    const { C } = this.tileInfo.cellProps;
    let lengthSum = 0;

    for (let i = 0; i < C.d.length - 1; i++) {
      C.z.push(lengthSum + C.d[i] / 2);
      lengthSum += C.z && C.d[i];
    }
    C.z[9] = 3.511845;
  }

  private findZCoordinatesForD(): void {
    const { D } = this.tileInfo.cellProps;
    let lengthSum = 0;

    for (let i = 0; i < D.d.length; i++) {
      if (i === 0) {
        D.z.push(0);
        lengthSum += D.d[0] / 2;
      } else {
        D.z.push(lengthSum + D.d[i] / 2);

        if (i === 4) {
          lengthSum = 3.5745 - D.d[4];
          D.z[4] = 3.4045;
        }

        lengthSum += D.d[i];
      }
    }
  }

  private setZCoordinates(): void {
    this.findZCoordinatesForA();
    this.findZCoordinatesForB();
    this.findZCoordinatesForC();
    this.findZCoordinatesForD();
  }

  private setNameAndThetaValues(): void {
    const { theta, eta, name } = this.tileInfo;
    const { sampling, tower } = this.tileInfo.tileComponents;

    for (let i = 0; i < this.tileInfo.count; i++) {
      const samplingValue = sampling.data[i];
      const towerValue = tower.data[i];

      theta.push(2 * Math.atan(Math.pow(Math.E, -eta[i])));

      switch (samplingValue) {
        case 0:
          name[i] = "A" + (towerValue + 1) * Math.sign(eta[i]);
          break;
        case 1:
          theta.push(2 * Math.atan(Math.pow(Math.E, -eta[i])));
          if (tower.data[i] < 8) name[i] = "BC" + (tower.data[i] + 1) * Math.sign(eta[i]);
          else if (tower.data[i] === 9) name[i] = "C" + 10 * Math.sign(eta[i]);
          else name[i] = "B" + (tower.data[i] + 1) * Math.sign(eta[i]);
          break;
        case 2:
          theta.push(2 * Math.atan(Math.pow(Math.E, -eta[i])));
          name[i] = "D" + (tower.data[i] / 2) * Math.sign(eta[i]);
          break;
        default:
          theta.push(2 * Math.atan(Math.pow(Math.E, -eta[i])));
          switch (tower.data[i]) {
            case 13:
              name[i] = "E" + 3 * Math.sign(eta[i]);
              break;
            case 15:
              name[i] = "E" + 4 * Math.sign(eta[i]);
              break;
            default:
              name[i] = "E" + (tower.data[i] % 9) * Math.sign(eta[i]);
          }
      }
    }
  }

  private drawLBA(layer: number): void {
    const { count, eta } = this.tileInfo;
    const { section, sampling } = this.tileInfo.tileComponents;

    if (layer !== -1) {
      for (let i = 0; i < count; i++) {
        if (
          (section.data[i] * Math.sign(eta[i]) == 1 || eta[i] == 0) &&
          sampling.data[i] == layer
        ) {
          this.cellGeoNCenter(i);
        }
      }
    } else {
      for (let i = 0; i < count; i++) {
        if (section.data[i] * Math.sign(eta[i]) == 1 || eta[i] == 0) {
          this.cellGeoNCenter(i);
        }
      }
    }
  }

  private drawLBC(layer: number): void {
    const { count, eta } = this.tileInfo;
    const { section, sampling } = this.tileInfo.tileComponents;

    if (layer !== -1) {
      for (let i = 0; i < count; i++) {
        if (section.data[i] * Math.sign(eta[i]) == -1 && sampling.data[i] == layer) {
          this.cellGeoNCenter(i);
        }
      }
    } else {
      for (let i = 0; i < count; i++) {
        if (section.data[i] * Math.sign(eta[i]) == -1) {
          this.cellGeoNCenter(i);
        }
      }
    }
  }

  private drawEBC(layer: number): void {
    const { count, eta } = this.tileInfo;
    const { section, sampling } = this.tileInfo.tileComponents;

    if (layer !== -1) {
      for (let i = 0; i < count; i++) {
        if (
          (section.data[i] * Math.sign(eta[i]) == -2 ||
            section.data[i] * Math.sign(eta[i]) == -3) &&
          sampling.data[i] == layer
        ) {
          this.cellGeoNCenter(i);
        }
      }
    } else {
      for (let i = 0; i < count; i++) {
        if (
          section.data[i] * Math.sign(eta[i]) == -2 ||
          section.data[i] * Math.sign(eta[i]) === -3
        ) {
          this.cellGeoNCenter(i);
        }
      }
    }
  }

  private drawEBA(layer: number): void {
    const { count, eta } = this.tileInfo;
    const { section, sampling } = this.tileInfo.tileComponents;

    if (layer !== -1) {
      for (let i = 0; i < count; i++) {
        if (
          (section.data[i] * Math.sign(eta[i]) == 2 || section.data[i] * Math.sign(eta[i]) == 3) &&
          sampling.data[i] == layer
        ) {
          this.cellGeoNCenter(i);
        }
      }
    } else {
      for (let i = 0; i < count; i++) {
        if (section.data[i] * Math.sign(eta[i]) == 2 || section.data[i] * Math.sign(eta[i]) == 3) {
          this.cellGeoNCenter(i);
        }
      }
    }
  }

  private drawDefault(): void {
    const { count } = this.tileInfo;

    for (let i = 0; i < count; i++) {
      this.cellGeoNCenter(i);
    }
  }

  private getLayerIndex(layer: string): number {
    const layerMap: Record<string, number> = {
      a: 0,
      bc: 1,
      d: 2,
      e: 3,
    };

    if (!(layer in layerMap)) {
      throw new Error("Invalid layer");
    }

    return layerMap[layer];
  }
  private getPartitionName(index: number): string {
    const layerMap: Record<string, string> = {
      "0": "+LBA",
      "1": "-LBC",
      "2": "+EBA",
      "3": "-EBC",
    };

    if (!(index.toString() in layerMap)) {
      throw new Error("Invalid layer");
    }

    return layerMap[index.toString()];
  }

  private drawLayer({ partition, name: layer }: ActiveCell): void {
    switch (partition) {
      case "LBA":
        this.drawLBA(this.getLayerIndex(layer));
        break;
      case "LBC":
        this.drawLBC(this.getLayerIndex(layer));
        break;
      case "EBA":
        this.drawEBA(this.getLayerIndex(layer));
        break;
      case "EBC":
        this.drawEBC(this.getLayerIndex(layer));
        break;
      default:
        this.drawDefault();
    }
  }

  private findCellCenter(i: number, radius: number, cellZ: number): void {
    const { theta, phi, eta, centerCoords } = this.tileInfo;

    const l = radius / Math.sin(theta[i]);
    const x = l * Math.sin(theta[i]) * Math.cos(phi[i]);
    const y = l * Math.sin(theta[i]) * Math.sin(phi[i]);
    const z = cellZ * Math.sign(eta[i]);

    centerCoords[i] = new Vector3(x, y, z);
  }

  private createGeo(
    i: number,
    widths: number[],
    height: number,
    depth: number,
    radius: number,
    inputZ: number,
    isSecondPmt: boolean,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    CinBC?: number
  ): void {
    const {
      eta,
      phi,
      centerCoords,
      energy,
      theta,
      name,
      pmt1Energy,
      pmt2Energy,
      pmt1Time,
      pmt2Time,
      fisheye,
      pmt,
      cellType,
      sectionName,
    } = this.tileInfo;
    const { section, module } = this.tileInfo.tileComponents;

    this.findCellCenter(i, radius, inputZ);

    let vertices: Float32Array;
    if (this.visualisationType === "parameter alongside eta") {
      vertices = isSecondPmt
        ? getRightPmtGeometryVertices(widths, height, depth)
        : getLeftPmtGeometryVertices(widths, height, depth);
    } else {
      vertices = getCellGeometryVertices(widths, height, depth);
    }

    const geometry = new BufferGeometry();
    geometry.setAttribute("position", new Float32BufferAttribute(vertices, 3));

    const edgeCell =
      fisheye && ["A1", "A-10", "A10", "A-1", "A16", "E3", "A-16", "E-3"].includes(name[i])
        ? true
        : false;

    // if (CinBC) {
    //   name[i] = (CinBC * Math.sign(eta[i])).toString() + i + sampling.data[i];
    // } else {
    //   name[i] = (section.data[i] * Math.sign(eta[i])).toString() + i + sampling.data[i];
    // }

    const position = new Vector3(
      centerCoords[i].x,
      centerCoords[i].y,
      centerCoords[i].z - depth / 2
    );

    const userData: CellData = {
      sectionName: sectionName[i],
      module: module.data[i],
      towers: this.getTowerNumbers(i),
      name: name[i],
      id: uid().toString(),
      section: section.data[i],
      object: "Tile Cell",
      cellType: cellType[i],
      energy: this.cellColorMode === "total energies" ? energy[i] : undefined,
      "calibration constants":
        this.cellColorMode === "calibration constants per channel"
          ? this.getCalibConstantsWithCellIndex(i)
          : undefined,
      noiseConstant:
        this.cellColorMode === "noise per cell"
          ? [this.selectedConstant, this.getNoisePerCellWithCellIndex(i)]
          : undefined,
      phi: phi[i],
      eta: eta[i],
      theta: theta[i],
      radius: radius,
      position: position,
      channels: this.tileInfo.channels[i],
      pmts: pmt[i],
      pmt1Energy:
        this.cellColorMode === "pmt1/pmt2 energies" ? pmt1Energy[i]?.toFixed(2) : undefined,
      pmt2Energy:
        this.cellColorMode === "pmt1/pmt2 energies" ? pmt2Energy[i]?.toFixed(2) : undefined,
      pmt1Time: this.cellColorMode === "pmt1/pmt2 energies" ? pmt1Time[i]?.toFixed(2) : undefined,
      pmt2Time: this.cellColorMode === "pmt1/pmt2 energies" ? pmt2Time[i]?.toFixed(2) : undefined,
    };

    let material: ShaderMaterial;
    let cellColors: [Color, Color];

    if (this.visualisationType === "parameter alongside eta") {
      cellColors = this.getMaterialByMode(i, userData, isSecondPmt ? 1 : 0);
      material = this.setCellMaterial(depth, widths, height, ...cellColors, edgeCell, "pmt");
    } else {
      cellColors = this.getMaterialByMode(i, userData);
      material = this.setCellMaterial(depth, widths, height, ...cellColors, edgeCell);
    }
    const mesh = new Mesh(geometry, material);
    mesh.position.set(position.x, position.y, position.z);
    mesh.rotation.set(0, 0, phi[i] + Math.PI / 2);

    mesh.userData = userData;
    this.tileInfo.geometries.push(mesh);
  }

  private setCellColor(cellEnergy: number): Color {
    const minEnergy = this.tileInfo.minEnergy;
    const maxEnergy = this.tileInfo.maxEnergy;
    const colorSchemeInterval = Math.abs(minEnergy - maxEnergy) / 8;
    const maxSchemeIndx = 8;

    for (let i = 0; i <= maxSchemeIndx; i++) {
      const col = minEnergy + colorSchemeInterval * i + colorSchemeInterval / 2;

      if (cellEnergy <= col) {
        return new Color(this.energyColorScheme[i]).convertLinearToSRGB();
      }
    }

    return new Color(this.energyColorScheme[maxSchemeIndx]).convertLinearToSRGB();
  }

  private getMaterialByMode(
    i: number,
    cellData: CellData,
    towerIndex: number | null = null
  ): [Color, Color] {
    const { parametersPerTower, energy } = this.tileInfo;
    const { module } = this.tileInfo.tileComponents;

    if (this.visualisationType === "parameter alongside eta") {
      if (towerIndex === null) throw new Error("Invalid tower index");
      const tower = this.getTowerNumbers(i)[towerIndex]
        ? this.getTowerNumbers(i)[towerIndex]
        : this.getTowerNumbers(i)[0];
      const energyColor = this.setCellColor(parametersPerTower[module.data[i] - 1][tower]);
      return [energyColor, energyColor];
    }

    switch (this.cellColorMode) {
      case "total energies": {
        const energyColor = this.setCellColor(energy[i]);
        return [energyColor, energyColor];
      }
      case "pmt1/pmt2 energies": {
        const energyColor1 = this.setCellColor(Number(cellData.pmt1Energy));
        const energyColor2 = this.setCellColor(Number(cellData.pmt2Energy));
        return [energyColor1, energyColor2];
      }
      case "calibration constants per channel": {
        const energyColor1 = this.setCellColor(this.getCalibConstantsWithCellIndex(i)[0]);

        const energyColor2 = this.setCellColor(
          this.getCalibConstantsWithCellIndex(i)[1] || this.getCalibConstantsWithCellIndex(i)[0]
        );
        return [energyColor1, energyColor2];
      }
      case "noise per cell": {
        try {
          const cellColor = this.setCellColor(this.getNoisePerCellWithCellIndex(i));
          return [cellColor, cellColor];
        } catch (err) {
          console.log(err);
          return [new Color(0xffffff), new Color(0xffffff)];
        }
      }
      default:
        throw new Error("Invalid color mode");
    }
  }

  private setCellMaterial(
    depth: number,
    width: number[],
    height: number,
    color1: Color,
    color2?: Color,
    edgeCell = false,
    cellType: "default" | "pmt" = "default"
  ): ShaderMaterial {
    const type = cellType === "default" ? 0 : 1;
    return new ShaderMaterial({
      transparent: true,
      uniforms: {
        thickness: {
          value: 0.07,
        },
        color1: {
          value: color1,
        },
        color2: {
          value: color2,
        },
        opacity: {
          value: 1,
        },
        edge_color: {
          value: new Color(0x103138),
        },
        depth: {
          value: depth / 2,
        },
        width1: {
          value: width[0] / 2,
        },
        width2: {
          value: width[1] / 2,
        },
        height: {
          value: height / 2,
        },
        edgeGap: {
          value: edgeCell ? 0.02 : 0.01,
        },
        type: {
          value: type,
        },
      },
      vertexShader: vertexShader,
      fragmentShader: fragmentShader,
    });
  }

  private cellGeoNCenter(index: number): void {
    const { cellProps, eta } = this.tileInfo;
    const { section, sampling, tower, module } = this.tileInfo.tileComponents;

    const createGeometry = (
      width: number[],
      height: number,
      depth: number,
      radius: number,
      inputZ: number,
      cinBC?: number
    ): void => {
      this.radius[index] = radius;

      if (this.visualisationType === "parameter alongside eta") {
        this.createGeo(index, width, height, depth, radius, inputZ, true, cinBC);
        this.createGeo(index, width, height, depth, radius, inputZ, false, cinBC);
      } else if (this.visualisationType === "default") {
        this.createGeo(index, width, height, depth, radius, inputZ, false, cinBC);
      } else {
        throw new Error("Invalid visualisation type");
      }
    };

    switch (section.data[index]) {
      case 1:
        switch (sampling.data[index]) {
          case 0:
            createGeometry(
              cellProps.A.w,
              cellProps.A.h,
              cellProps.A.d[tower.data[index]],
              cellProps.A.r,
              cellProps.A.z[tower.data[index]]
            );
            break;
          case 1:
            if (tower.data[index] === 8) {
              createGeometry(
                cellProps.BLB.w,
                cellProps.BLB.h,
                cellProps.B.d[tower.data[index]],
                cellProps.BLB.r,
                cellProps.B.z[tower.data[index]]
              );
            } else {
              createGeometry(
                cellProps.BLB.w,
                cellProps.BLB.h,
                cellProps.B.d[tower.data[index]],
                cellProps.BLB.r,
                cellProps.B.z[tower.data[index]]
              );
              createGeometry(
                cellProps.C.w,
                cellProps.C.h,
                cellProps.C.d[tower.data[index]],
                cellProps.C.r,
                cellProps.C.z[tower.data[index]],
                4
              );
            }
            break;
          default:
            createGeometry(
              cellProps.DLBD4.w,
              cellProps.DLBD4.h,
              cellProps.D.d[tower.data[index] / 2],
              cellProps.DLBD4.r,
              cellProps.D.z[tower.data[index] / 2]
            );
        }
        break;
      case 2:
        switch (sampling.data[index]) {
          case 0:
            createGeometry(
              cellProps.A.w,
              cellProps.A.h,
              cellProps.A.d[tower.data[index]],
              cellProps.A.r,
              cellProps.A.z[tower.data[index]]
            );
            break;
          case 1:
            createGeometry(
              cellProps.BEB.w,
              cellProps.BEB.h,
              cellProps.B.d[tower.data[index]],
              cellProps.BEB.r,
              cellProps.B.z[tower.data[index]]
            );
            break;
          default: {
            if (
              (module.data[index] === 14 && eta[index] > 0 && eta[index] < 1.2) ||
              (module.data[index] === 17 && eta[index] < 0 && eta[index] > -1.2)
            ) {
              createGeometry(
                cellProps.DEB.w,
                cellProps.DEB.h,
                cellProps.D.d[tower.data[index] / 2] +
                  cellProps.C.d[cellProps.C.d.length - 1] +
                  0.01,
                cellProps.DEB.r,
                cellProps.D.z[tower.data[index] / 2] -
                  cellProps.C.d[cellProps.C.d.length - 1] +
                  0.04
              );
            } else {
              createGeometry(
                cellProps.DEB.w,
                cellProps.DEB.h,
                cellProps.D.d[tower.data[index] / 2],
                cellProps.DEB.r,
                cellProps.D.z[tower.data[index] / 2]
              );
            }
          }
        }
        break;
      default:
        switch (sampling.data[index]) {
          case 1:
            {
              if (
                ((module.data[index] === -38 ||
                  module.data[index] === -39 ||
                  module.data[index] === -40 ||
                  module.data[index] === -41) &&
                  eta[index] > 0) ||
                ((module.data[index] === -54 ||
                  module.data[index] === -55 ||
                  module.data[index] === -56 ||
                  module.data[index] === -57) &&
                  eta[index] < 0)
              ) {
                createGeometry(
                  cellProps.C.w,
                  cellProps.C.h,
                  cellProps.E.d[0],
                  cellProps.C.r,
                  cellProps.E.z[0]
                );
              } else {
                createGeometry(
                  cellProps.C.w,
                  cellProps.C.h,
                  cellProps.C.d[tower.data[index]],
                  cellProps.C.r,
                  cellProps.C.z[9]
                );
              }
            }
            break;
          case 2: {
            if (
              ((module.data[index] === 13 ||
                module.data[index] === 17 ||
                module.data[index] === 18) &&
                eta[index] > 0 &&
                eta[index] < 1) ||
              ((module.data[index] === 13 ||
                module.data[index] === 14 ||
                module.data[index] === 18) &&
                eta[index] < 0 &&
                eta[index] > -1)
            ) {
              createGeometry(
                cellProps.DLBD4.w,
                cellProps.DLBD4.h,
                cellProps.C.d[cellProps.C.d.length - 1],
                cellProps.DLBD4.r,
                cellProps.C.z[9]
              );
            } else {
              createGeometry(
                cellProps.DLBD4.w,
                cellProps.DLBD4.h,
                cellProps.D.d[tower.data[index] / 2],
                cellProps.DLBD4.r,
                cellProps.D.z[tower.data[index] / 2]
              );
            }
            break;
          }
          default: {
            if (tower.data[index] === 10) {
              createGeometry(
                cellProps.E1.w,
                cellProps.E1.h,
                cellProps.E.d[0],
                cellProps.E1.r,
                cellProps.E.z[0]
              );
            } else if (tower.data[index] === 11) {
              createGeometry(
                cellProps.E2.w,
                cellProps.E2.h,
                cellProps.E.d[0],
                cellProps.E2.r,
                cellProps.E.z[0]
              );
            } else {
              createGeometry(
                cellProps.E3.w,
                cellProps.E3.h,
                cellProps.E.d[1],
                cellProps.E3.r,
                Math.sign(cellProps.E.z[1]) * (Math.abs(cellProps.E.z[1]) - 0.002)
              );
            }
          }
        }
    }
  }
  private setChannelsToAllCells(): void {
    const { module: moduleNumber, section } = this.tileInfo.tileComponents;
    const { eta, name } = this.tileInfo;

    moduleNumber.data.forEach((module, index) => {
      moduleNumber.data[index] = Math.abs(module) + 1;

      switch (section.data[index]) {
        case 1:
          if (eta[index] > 0) this.tileInfo.sectionName[index] = "+LBA";
          else this.tileInfo.sectionName[index] = "-LBC";
          if (name[index].indexOf("-") == -1) {
            this.tileInfo.channels[index] =
              channelsMap["LBA-LBC"][name[index] as keyof (typeof channelsMap)["LBA-LBC"]];
            this.tileInfo.pmt[index] =
              pmtsMap["LBA-LBC"][name[index] as keyof (typeof pmtsMap)["LBA-LBC"]];
          } else {
            const modifiedName = name[index].replace(
              "-",
              ""
            ) as keyof (typeof channelsMap)["LBA-LBC"];
            this.tileInfo.channels[index] = channelsMap["LBA-LBC"][modifiedName];
            this.tileInfo.pmt[index] = pmtsMap["LBA-LBC"][modifiedName];
          }
          break;
        case 2:
          if (eta[index] > 0) {
            this.tileInfo.sectionName[index] = "+EBA";
            if (module == 15) {
              this.tileInfo.channels[index] =
                channelsMap["EBA15-EBC18"][
                  name[index] as keyof (typeof channelsMap)["EBA15-EBC18"]
                ];
              this.tileInfo.pmt[index] =
                pmtsMap["EBA15-EBC18"][name[index] as keyof (typeof pmtsMap)["EBA15-EBC18"]];
              break;
            }
          } else {
            this.tileInfo.sectionName[index] = "-EBC";
            if (module == 18) {
              const modifiedName = name[index].replace(
                "-",
                ""
              ) as keyof (typeof channelsMap)["EBA15-EBC18"];
              const modifiedName2 = name[index].replace(
                "-",
                ""
              ) as keyof (typeof pmtsMap)["EBA15-EBC18"];
              this.tileInfo.channels[index] = channelsMap["EBA15-EBC18"][modifiedName];
              this.tileInfo.pmt[index] = pmtsMap["EBA15-EBC18"][modifiedName2];

              break;
            }
          }
          if (name[index].indexOf("-") == -1) {
            this.tileInfo.channels[index] =
              channelsMap["EBA-EBC"][name[index] as keyof (typeof channelsMap)["EBA-EBC"]];
            this.tileInfo.pmt[index] =
              pmtsMap["EBA-EBC"][name[index] as keyof (typeof pmtsMap)["EBA-EBC"]];
          } else {
            const modifiedName = name[index].replace(
              "-",
              ""
            ) as keyof (typeof channelsMap)["EBA-EBC"];
            this.tileInfo.channels[index] = channelsMap["EBA-EBC"][modifiedName];
            this.tileInfo.pmt[index] = pmtsMap["EBA-EBC"][modifiedName];
          }
          break;
        default:
          if (eta[index] > 0) {
            this.tileInfo.sectionName[index] = "+EBA";
          } else {
            this.tileInfo.sectionName[index] = "-EBC";
          }
          if (name[index].indexOf("-") == -1) {
            this.tileInfo.channels[index] =
              channelsMap["EBA-EBC"][name[index] as keyof (typeof channelsMap)["EBA-EBC"]];
            this.tileInfo.pmt[index] =
              pmtsMap["EBA-EBC"][name[index] as keyof (typeof pmtsMap)["EBA-EBC"]];
          } else {
            const modifiedName = name[index].replace(
              "-",
              ""
            ) as keyof (typeof channelsMap)["EBA-EBC"];
            this.tileInfo.channels[index] = channelsMap["EBA-EBC"][modifiedName];
            this.tileInfo.pmt[index] = pmtsMap["EBA-EBC"][modifiedName];
          }
      }
    });
  }
  private getCalibConstantsWithCellIndex(i: number): [number] | [number, number] {
    const { parameterPerChannel, selectedConstant, selectedGain } = this;
    const { sectionName, channels } = this.tileInfo;
    const { module } = this.tileInfo.tileComponents;
    const section = sectionName[i].slice(1);
    const moduleNumber = module.data[i].toString().padStart(2, "0");
    const constantNumber = Number(selectedConstant) as number;
    const calibConstant1 =
      parameterPerChannel[section][moduleNumber][channels[i][0]][selectedGain][constantNumber];
    const calibConstant2 =
      channels[i][1] !== undefined
        ? parameterPerChannel[section][moduleNumber][channels[i][1]][selectedGain][constantNumber]
        : null;
    return calibConstant2 !== null ? [calibConstant1, calibConstant2] : [calibConstant1];
  }
  private getNoisePerCellWithCellIndex(i: number): number {
    const { parameterPerCell } = this;
    const { sectionName, name } = this.tileInfo;
    const { module } = this.tileInfo.tileComponents;
    const section = sectionName[i].slice(1);
    const moduleNumber = module.data[i].toString().padStart(2, "0");
    const noisePerCell =
      parameterPerCell[section][moduleNumber][name[i]][this.selectedGain][this.selectedConstant];
    return noisePerCell;
  }
}
