import {
  BufferGeometry,
  Color,
  Float32BufferAttribute,
  Mesh,
  ShaderMaterial,
} from "three";

import { ActiveModule, ModuleData } from "#/types/app.types";
import { fragmentShader, vertexShader } from "#/lib/shaders/shader";
import { uid } from "#/utils/uid";

export type TileModuleType = Mesh<BufferGeometry, ShaderMaterial>;

export default class TileModule {
  modules: TileModuleType[] = [];

  drawTileModules(modules: ActiveModule[]): this {
    modules.forEach((module) => {
      const moduleNumber = Number(module.name.slice(-2));
      this.createModule(moduleNumber, module.partition);
    });

    return this;
  }

  getModules(): TileModuleType[] {
    return this.modules;
  }

  private formModuleGeometry(
    section: string,
    i: number,
    btmW: number,
    topW: number,
    h: number,
    d: number,
    y: number,
    z: number
  ): void {
    i -= 1;
    const w = [];
    w[0] = topW;
    w[1] = btmW;
    const vertices = new Float32Array([
      //! z=0
      w[1] / 2,
      -h / 2,
      0,
      -w[1] / 2,
      -h / 2,
      0,
      w[0] / 2,
      h / 2,
      0,

      w[0] / 2,
      h / 2,
      0,
      -w[1] / 2,
      -h / 2,
      0,
      -w[0] / 2,
      h / 2,
      0,
      //! z=d
      w[1] / 2,
      -h / 2,
      d,
      w[0] / 2,
      h / 2,
      d,
      -w[1] / 2,
      -h / 2,
      d,

      w[0] / 2,
      h / 2,
      d,
      -w[0] / 2,
      h / 2,
      d,
      -w[1] / 2,
      -h / 2,
      d,
      //! xy qveda
      w[1] / 2,
      -h / 2,
      0,
      w[1] / 2,
      -h / 2,
      d,
      -w[1] / 2,
      -h / 2,
      0,

      w[1] / 2,
      -h / 2,
      d,
      -w[1] / 2,
      -h / 2,
      d,
      -w[1] / 2,
      -h / 2,
      0,
      //! xy zeda
      w[0] / 2,
      h / 2,
      0,
      -w[0] / 2,
      h / 2,
      0,
      w[0] / 2,
      h / 2,
      d,

      w[0] / 2,
      h / 2,
      d,
      -w[0] / 2,
      h / 2,
      0,
      -w[0] / 2,
      h / 2,
      d,
      //! xy marjvniv
      -w[0] / 2,
      h / 2,
      0,
      -w[1] / 2,
      -h / 2,
      0,
      -w[0] / 2,
      h / 2,
      d,

      -w[0] / 2,
      h / 2,
      d,
      -w[1] / 2,
      -h / 2,
      0,
      -w[1] / 2,
      -h / 2,
      d,
      //! xy marcxniv
      w[0] / 2,
      h / 2,
      0,
      w[0] / 2,
      h / 2,
      d,
      w[1] / 2,
      -h / 2,
      d,

      w[0] / 2,
      h / 2,
      0,
      w[1] / 2,
      -h / 2,
      d,
      w[1] / 2,
      -h / 2,
      0,
    ]);

    const geometry = new BufferGeometry();
    geometry.setAttribute("position", new Float32BufferAttribute(vertices, 3));

    const material = new ShaderMaterial({
      uniforms: {
        thickness: {
          value: 0.07,
        },
        color1: {
          value: new Color("rgb(98,49,56)").convertLinearToSRGB(),
        },
        color2: {
          value: new Color("rgb(98,49,56)").convertLinearToSRGB(),
        },
        opacity: {
          value: 1,
        },
        edge_color: {
          value: new Color(0x103138),
        },
        depth: {
          value: d / 2,
        },
        width1: {
          value: w[0] / 2,
        },
        width2: {
          value: w[1] / 2,
        },
        height: {
          value: h / 2,
        },
      },
      vertexShader: vertexShader,
      fragmentShader: fragmentShader,
    });

    const mesh = new Mesh(geometry, material);
    mesh.name = section + i;

    if (section[section.length - 1] === "A") section = "+" + section;
    else section = "-" + section;

    mesh.material.transparent = true;
    mesh.position.z = z;
    mesh.rotation.z = (i * Math.PI) / 32 + Math.PI / 64 - Math.PI / 2;
    mesh.translateY(y);

    mesh.userData = {
      id: uid().toString(),
      name: section + i,
      object: "TileCal Module",
      section: section,
      module: i,
    } as ModuleData;

    this.modules.push(mesh);
  }

  private createModule(i: number, section: string): void {
    const h = 1.52;
    const bottomW = 0.2236208717388582;
    const topW = 0.37272683914888616;
    const LBDepth = 2.80254;
    const LBY = 2.3 + h / 2;
    const EBDepth = 2.5555;
    const EBCenter = 3.575;
    const DITCH = 0.38;
    const DITCW = 0.30602153793913683;
    const DDepthITC = 0.309;
    const DCenterITC = 3.2505;
    const DITCY = 3.63;
    const CITCH = 0.45;
    const CITCWBtm = 0.2913071332605156;
    const CITCTop = 0.3354503472963792;
    const CDepthITC = 0.09471;
    const CCenterITC = 3.46449;
    const CITCY = 3.215;
    const E1n2H = 0.654;
    const E1n2WBtm = 0.25756209853087775;
    const E1n2Top = 0.2882661562936006;
    const E1n2DepthITC = 0.015;
    const E1n2CenterITC = 3.5445;
    const E1n2Y = 2.632;
    const E3n4H = 0.81;
    const E3n4Btm = 0.14171068569453354;
    const E3n4Top = 0.22411135189481218;
    const E3n4DepthITC = 0.008;
    const E3n4CenterITC = 3.532;
    const E3n4Y = 1.885;

    switch (section) {
      case "LBA":
        this.formModuleGeometry(section, i, bottomW, topW, h, LBDepth, LBY, 0);
        break;
      case "LBC":
        this.formModuleGeometry(
          section,
          i,
          bottomW,
          topW,
          h,
          LBDepth,
          LBY,
          -LBDepth
        );
        break;
      case "EBA":
        this.formModuleGeometry(
          section,
          i,
          bottomW,
          topW,
          h,
          EBDepth,
          LBY,
          EBCenter
        );
        this.formModuleGeometry(
          section,
          i,
          DITCW,
          topW,
          DITCH,
          DDepthITC,
          DITCY,
          DCenterITC
        );
        this.formModuleGeometry(
          section,
          i,
          CITCWBtm,
          CITCTop,
          CITCH,
          CDepthITC,
          CITCY,
          CCenterITC
        );
        this.formModuleGeometry(
          section,
          i,
          E1n2WBtm,
          E1n2Top,
          E1n2H,
          E1n2DepthITC,
          E1n2Y,
          E1n2CenterITC
        );
        this.formModuleGeometry(
          section,
          i,
          E3n4Btm,
          E3n4Top,
          E3n4H,
          E3n4DepthITC,
          E3n4Y,
          E3n4CenterITC
        );
        break;
      case "EBC":
        this.formModuleGeometry(
          section,
          i,
          bottomW,
          topW,
          h,
          EBDepth,
          LBY,
          -EBCenter - EBDepth
        );
        this.formModuleGeometry(
          section,
          i,
          DITCW,
          topW,
          DITCH,
          DDepthITC,
          DITCY,
          -DCenterITC - DDepthITC
        );
        this.formModuleGeometry(
          section,
          i,
          CITCWBtm,
          CITCTop,
          CITCH,
          CDepthITC,
          CITCY,
          -CCenterITC - CDepthITC
        );
        this.formModuleGeometry(
          section,
          i,
          E1n2WBtm,
          E1n2Top,
          E1n2H,
          E1n2DepthITC,
          E1n2Y,
          -E1n2CenterITC - E1n2DepthITC
        );
        this.formModuleGeometry(
          section,
          i,
          E3n4Btm,
          E3n4Top,
          E3n4H,
          E3n4DepthITC,
          E3n4Y,
          -E3n4CenterITC - E3n4DepthITC
        );
        break;
      case "all":
        this.formModuleGeometry(section, i, bottomW, topW, h, LBDepth, LBY, 0);
        this.formModuleGeometry(
          section,
          i,
          bottomW,
          topW,
          h,
          LBDepth,
          LBY,
          -LBDepth
        );
        this.formModuleGeometry(
          section,
          i,
          bottomW,
          topW,
          h,
          EBDepth,
          LBY,
          EBCenter
        );
        this.formModuleGeometry(
          section,
          i,
          DITCW,
          topW,
          DITCH,
          DDepthITC,
          DITCY,
          DCenterITC
        );
        this.formModuleGeometry(
          section,
          i,
          CITCWBtm,
          CITCTop,
          CITCH,
          CDepthITC,
          CITCY,
          CCenterITC
        );
        this.formModuleGeometry(
          section,
          i,
          E1n2WBtm,
          E1n2Top,
          E1n2H,
          E1n2DepthITC,
          E1n2Y,
          E1n2CenterITC
        );
        this.formModuleGeometry(
          section,
          i,
          E3n4Btm,
          E3n4Top,
          E3n4H,
          E3n4DepthITC,
          E3n4Y,
          E3n4CenterITC
        );
        this.formModuleGeometry(
          section,
          i,
          bottomW,
          topW,
          h,
          EBDepth,
          LBY,
          -EBCenter - EBDepth
        );
        this.formModuleGeometry(
          section,
          i,
          DITCW,
          topW,
          DITCH,
          DDepthITC,
          DITCY,
          -DCenterITC - DDepthITC
        );
        this.formModuleGeometry(
          section,
          i,
          CITCWBtm,
          CITCTop,
          CITCH,
          CDepthITC,
          CITCY,
          -CCenterITC - CDepthITC
        );
        this.formModuleGeometry(
          section,
          i,
          E1n2WBtm,
          E1n2Top,
          E1n2H,
          E1n2DepthITC,
          E1n2Y,
          -E1n2CenterITC - E1n2DepthITC
        );
        this.formModuleGeometry(
          section,
          i,
          E3n4Btm,
          E3n4Top,
          E3n4H,
          E3n4DepthITC,
          E3n4Y,
          -E3n4CenterITC - E3n4DepthITC
        );
        break;
      default:
        alert("Invalid section name!");
    }
  }
}
