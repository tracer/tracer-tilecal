import { CellData } from "#/types/app.types";
import { channelsMap } from "#/constants/Channels";

export default class Channels {
  setChannelsToCell(cellData: CellData): void {
    const { name, module, section, eta } = cellData;
    cellData.module = Math.abs(module) + 1;
    // console.log(name);

    switch (section) {
      case 1:
        if (eta > 0) cellData.sectionName = "+LBA";
        else cellData.sectionName = "-LBC";
        if (name.indexOf("-") == -1) {
          cellData.channels = channelsMap["LBA-LBC"][name as keyof (typeof channelsMap)["LBA-LBC"]];
        } else {
          const modifiedName = name.replace("-", "") as keyof (typeof channelsMap)["LBA-LBC"];
          cellData.channels = channelsMap["LBA-LBC"][modifiedName];
        }
        break;
      case 2:
        if (eta > 0) {
          cellData.sectionName = "+EBA";
          if (cellData.module == 15) {
            cellData.channels =
              channelsMap["EBA15-EBC18"][name as keyof (typeof channelsMap)["EBA15-EBC18"]];
            break;
          }
        } else {
          cellData.sectionName = "-EBC";
          if (cellData.module == 18) {
            const modifiedName = name.replace("-", "") as keyof (typeof channelsMap)["EBA15-EBC18"];
            cellData.channels = channelsMap["EBA15-EBC18"][modifiedName];
            break;
          }
        }
        if (name.indexOf("-") == -1) {
          cellData.channels = channelsMap["EBA-EBC"][name as keyof (typeof channelsMap)["EBA-EBC"]];
        } else {
          const modifiedName = name.replace("-", "") as keyof (typeof channelsMap)["EBA-EBC"];
          cellData.channels = channelsMap["EBA-EBC"][modifiedName];
        }
        break;
      default:
        if (eta > 0) {
          cellData.sectionName = "+ETCA";
        } else {
          cellData.sectionName = "-ETCC";
        }
        if (name.indexOf("-") == -1) {
          cellData.channels = channelsMap["EBA-EBC"][name as keyof (typeof channelsMap)["EBA-EBC"]];
        } else {
          const modifiedName = name.replace("-", "") as keyof (typeof channelsMap)["EBA-EBC"];
          cellData.channels = channelsMap["EBA-EBC"][modifiedName];
        }
    }
  }
}
