import Camera from "#/three/Camera.three";
import Loader from "#/three/CustomLoader.three";
import Detector from "#/three/Detector.three";
import Tile from "#/three/event/tile.three";
import TileModule from "#/three/event/tileModule.three";
import Grid from "#/three/Grid.three";
import Lights from "#/three/Light.three";
import RaycastContainer from "#/three/Raycast.three";
import StatsDispatcher from "#/three/Stats.three";
import { NoToneMapping } from "three";

import { Fisheye } from "@react-three/drei";
import { Canvas } from "@react-three/fiber";
import { lazy, Suspense } from "react";

import { selectDefaultCameraPosition } from "#/store/features/cameraSlice";
import { selectFisheye } from "#/store/features/globalsSlice";
import { selectGeometriesCutType, selectSnapIsLoading } from "#/store/features/modelSlice";
import { useAppSelector } from "#/store/hooks";

const Controls = lazy(() => import("#/three/Controls.three"));
const Axis = lazy(() => import("#/three/Axis.three"));
const Event = lazy(() => import("#/three/event/EventVisualisation.three"));
const ParticleSystem = lazy(() => import("#/three/particle-animation/ParticleSystem"));

export default function Scene() {
  const defaultPosition = useAppSelector(selectDefaultCameraPosition);
  const cutType = useAppSelector(selectGeometriesCutType);
  const snapIsLoading = useAppSelector(selectSnapIsLoading);
  const fisheyeMode = useAppSelector(selectFisheye);
  const localClippingEnabled = cutType === null;

  return (
    <>
      <Canvas
        flat
        gl={{
          pixelRatio: window.devicePixelRatio * 0.5,
          alpha: true,
          toneMapping: NoToneMapping,
          localClippingEnabled,
          preserveDrawingBuffer: true,
        }}
        linear
        id="canvas"
        frameloop="always"
        camera={{ manual: true, position: defaultPosition }}
      >
        <Suspense fallback={null}>
          {fisheyeMode && (
            <Fisheye zoom={0}>
              <RaycastContainer />
              <Lights />
              <Grid />
              <Tile />
              <TileModule />
              {!snapIsLoading && <Detector />}
              <Event />
              <ParticleSystem />
              <StatsDispatcher />
              <Camera />
              <Controls />
            </Fisheye>
          )}
          {!fisheyeMode && (
            <>
              <RaycastContainer />
              <Lights />
              <Grid />
              <Tile />
              <TileModule />
              {!snapIsLoading && <Detector />}
              <Axis />
              <Event />
              <ParticleSystem />
              <StatsDispatcher />
              <Camera />
              <Controls />
            </>
          )}
        </Suspense>
      </Canvas>
      <Loader />
    </>
  );
}
