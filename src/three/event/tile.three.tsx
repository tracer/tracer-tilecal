import { MeshType } from "#/models/Tile";

import { useEffect, useState } from "react";

import { ParameterPerCellObject, VisualisationType } from "#/types/app.types";
import { handleFilter } from "#/utils/handleFilter";
import { handleSelectionOpacity } from "#/utils/handleSelectionOpacity";
import { setPmtInfoExists } from "#/store/features/eventSlice";
import { selectSelectedObject } from "#/store/features/selection";
import {
  selectCellColorMode,
  selectColorSchemes,
  selectEnergyColorScheme,
  selectEnergySchemeIntervals,
  selectEtaAngleFilterLimits,
  selectFilterCellColorsMinMax,
  selectFixScale,
  selectParameterPerCell,
  selectParameterPerChannel,
  selectParameterPerChannelMinMax,
  selectPhiAngleFilterLimits,
  selectSelectedConstant,
  selectSelectedGain,
  selectThetaAngleFilterLimits,
  selectVisualisationType,
  setEnergyLimits,
} from "#/store/features/tcalSlice";
import { selectActiveCells } from "#/store/features/treeSlice";
import { useAppDispatch, useAppSelector } from "#/store/hooks";
import useEvent from "#/hooks/useEvent.hook";

export default function Tile() {
  const dispatch = useAppDispatch();
  const [cells, setCells] = useState<MeshType[]>([]);
  const { event, TILE } = useEvent();
  const activeCells = useAppSelector(selectActiveCells);
  const energyColorScheme = useAppSelector(selectEnergyColorScheme);
  const colorSchemes = useAppSelector(selectColorSchemes);
  const selectedObject = useAppSelector(selectSelectedObject);
  const colorFilter = useAppSelector(selectFilterCellColorsMinMax);
  const angleFilterTheta = useAppSelector(selectThetaAngleFilterLimits);
  const angleFilterPhi = useAppSelector(selectPhiAngleFilterLimits);
  const angleFilterEta = useAppSelector(selectEtaAngleFilterLimits);
  const cellColorMode = useAppSelector(selectCellColorMode);
  const energySchemeIntervals = useAppSelector(selectEnergySchemeIntervals);
  const fixScale = useAppSelector(selectFixScale);
  const parameterPerChannelMinMax = useAppSelector(selectParameterPerChannelMinMax);
  const parameterPerChannel = useAppSelector(selectParameterPerChannel);
  const selectedConstant = useAppSelector(selectSelectedConstant);
  const selectedGain = useAppSelector(selectSelectedGain);
  const selectedVisualisationType: VisualisationType = useAppSelector(selectVisualisationType);
  const parameterPerCell: ParameterPerCellObject = useAppSelector(selectParameterPerCell);
  const parameterPerChannelFileData = useAppSelector(selectParameterPerChannel);

  const parameterPerChannelInfo = {
    parameterPerChannel,
    parameterPerChannelMinMax,
  };

  useEffect(() => {
    if (event) {
      TILE.cellColorMode = cellColorMode;
      TILE.visualisationType = selectedVisualisationType;
      TILE.selectedGain = selectedGain;
      TILE.energyColorScheme = colorSchemes[energyColorScheme];
      TILE.selectedConstant = selectedConstant;
      TILE.parameterPerCell = parameterPerCell;
      const min = parseFloat(energySchemeIntervals[energySchemeIntervals.length - 1]?.value);
      const max = parseFloat(energySchemeIntervals[0]?.value);
      const options = !isNaN(min) && !isNaN(max) ? { min, max } : undefined;

      const geometries = TILE.init(event.Event.TILE, parameterPerChannelInfo)
        .setMinMaxEnergy(options, fixScale, false)
        .drawCells(activeCells)
        .getGeometries();

      dispatch(
        setEnergyLimits({
          min: TILE.tileInfo.minEnergy,
          max: TILE.tileInfo.maxEnergy,
        })
      );

      if (TILE.tileInfo.pmt1Energy && TILE.tileInfo.pmt1Energy.length > 0) {
        dispatch(setPmtInfoExists(true));
      } else {
        dispatch(setPmtInfoExists(false));
      }

      setCells(geometries);
      TILE.reset();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    event,
    TILE,
    cellColorMode,
    activeCells,
    selectedConstant,
    selectedVisualisationType,
    selectedGain,
    parameterPerChannelFileData,
  ]);

  useEffect(() => {
    if (event) {
      TILE.cellColorMode = cellColorMode;
      TILE.visualisationType = selectedVisualisationType;
      TILE.energyColorScheme = colorSchemes[energyColorScheme];

      const min = parseFloat(energySchemeIntervals[energySchemeIntervals.length - 1]?.value);
      const max = parseFloat(energySchemeIntervals[0]?.value);

      const options = !isNaN(min) && !isNaN(max) ? { min, max } : undefined;

      const geometries = TILE.init(event.Event.TILE)
        .setMinMaxEnergy(options, fixScale)
        .drawCells(activeCells)
        .getGeometries();

      if (TILE.tileInfo.pmt1Energy && TILE.tileInfo.pmt1Energy.length > 0) {
        dispatch(setPmtInfoExists(true));
      } else {
        dispatch(setPmtInfoExists(false));
      }

      setCells(geometries);
      TILE.reset();
    }
  }, [energySchemeIntervals]);

  useEffect(() => {
    if (event) {
      TILE.cellColorMode = cellColorMode;
      TILE.visualisationType = selectedVisualisationType;

      TILE.energyColorScheme = colorSchemes[energyColorScheme];
      const min = parseFloat(energySchemeIntervals[energySchemeIntervals.length - 1]?.value);
      const max = parseFloat(energySchemeIntervals[0]?.value);
      const options = !isNaN(min) && !isNaN(max) ? { min, max } : undefined;

      const geometries = TILE.init(event.Event.TILE)
        .setMinMaxEnergy(options, fixScale)
        .drawCells(activeCells)
        .getGeometries();
      dispatch(
        setEnergyLimits({
          min: TILE.tileInfo.minEnergy,
          max: TILE.tileInfo.maxEnergy,
        })
      );
      if (TILE.tileInfo.pmt1Energy && TILE.tileInfo.pmt1Energy.length > 0) {
        dispatch(setPmtInfoExists(true));
      } else {
        dispatch(setPmtInfoExists(false));
      }
      setCells(geometries);
      TILE.reset();
    }
  }, [energyColorScheme, colorSchemes.customScheme, TILE]);

  if (cells.length > 0) {
    return (
      <>
        {cells.map((mesh) => {
          handleSelectionOpacity(selectedObject, mesh);
          handleFilter({
            mesh,
            colorFilter,
            thetaFilter: angleFilterTheta.values,
            phiFilter: angleFilterPhi.values,
            etaFilter: angleFilterEta.values,
          });

          return <primitive key={mesh.userData.id} object={mesh} />;
        })}
      </>
    );
  }

  return <></>;
}
