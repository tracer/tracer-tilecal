import { TileModuleType } from "#/models/TileModule";

import { useEffect, useState } from "react";

import { handleSelectionOpacity } from "#/utils/handleSelectionOpacity";
import { selectSelectedObject } from "#/store/features/selection";
import { selectActiveModules } from "#/store/features/treeSlice";
import { useAppSelector } from "#/store/hooks";
import useEvent from "#/hooks/useEvent.hook";

export default function TileModule() {
  const [modules, setModules] = useState<TileModuleType[]>([]);
  const { TileModule } = useEvent();
  const activeModules = useAppSelector(selectActiveModules);
  const selectedObject = useAppSelector(selectSelectedObject);

  useEffect(() => {
    const modulesToDraw = TileModule.drawTileModules(activeModules).getModules();
    TileModule.modules = [];

    setModules(modulesToDraw);
  }, [activeModules, TileModule]);

  if (modules.length > 0) {
    return (
      <>
        {modules.map((mesh) => {
          handleSelectionOpacity(selectedObject, mesh);
          return <primitive key={mesh.userData.id} object={mesh} />;
        })}
      </>
    );
  }

  return <></>;
}
