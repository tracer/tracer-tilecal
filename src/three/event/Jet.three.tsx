import { useMemo } from "react";

import { selectEventDisabledObjects } from "#/store/features/eventSlice";
import { useAppSelector } from "#/store/hooks";
import useEvent from "#/hooks/useEvent.hook";

export default function Jet() {
  const { event, eventParameters, JET } = useEvent();
  const disabledEventObjects = useAppSelector(selectEventDisabledObjects);

  const jetConesToDraw = useMemo(() => {
    if (event && !disabledEventObjects?.includes("jet")) {
      if (Array.isArray(event.Event.Jet)) {
        JET.init(event.Event.Jet[0]);
      } else {
        JET.init(event.Event.Jet);
      }
      return JET.drawJetCone();
    }
    return null;
  }, [event, JET]);

  if (jetConesToDraw && eventParameters.jet) {
    return (
      <>
        {jetConesToDraw.map((jetCone, i) => {
          return (
            <mesh key={i} geometry={jetCone.geo} quaternion={jetCone.quaternion}>
              <meshToonMaterial
                attach="material"
                color="#e40cf0"
                transparent={true}
                opacity={0.7}
              />
            </mesh>
          );
        })}
      </>
    );
  }

  return <></>;
}
