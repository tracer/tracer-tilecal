import { combineReducers, configureStore } from "@reduxjs/toolkit";

import cameraReducer from "#/store/features/cameraSlice";
import eventReducer from "#/store/features/eventSlice";
import globalsReducer from "#/store/features/globalsSlice";
import modalsreducer from "#/store/features/modalSlice";
import modelReducer from "#/store/features/modelSlice";
import infoReducer from "#/store/features/rendererSlice";
import selectionReducer from "#/store/features/selection";
import tcalReducer from "#/store/features/tcalSlice";
import treeReducer from "#/store/features/treeSlice";

const combinedReducers = {
  globals: globalsReducer,
  renderer: infoReducer,
  camera: cameraReducer,
  modal: modalsreducer,
  tree: treeReducer,
  event: eventReducer,
  tcal: tcalReducer,
  selection: selectionReducer,
  model: modelReducer,
};

export const rootReducer = combineReducers(combinedReducers);

const store = configureStore({
  reducer: rootReducer,
  devTools: import.meta.env.VITE_ENV === "development",
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({ serializableCheck: false }),
});

export default store;
