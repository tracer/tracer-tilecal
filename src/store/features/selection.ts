import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { CellData, ModelInfo, ModuleData, RootState } from "#/types/app.types";

export type SelectedObject = undefined | ModuleData | CellData | ModelInfo;

type Selection = {
  object: SelectedObject;
  showModal: boolean;
};

const initialState: Selection = {
  object: undefined,
  showModal: false,
};

const modelSlice = createSlice({
  name: "selection",
  initialState,
  reducers: {
    rehydrate: (state, action) => {
      return action.payload.model || state;
    },

    setSelection: (state, action: PayloadAction<SelectedObject>) => {
      state.object = action.payload;

      action.payload ? (state.showModal = true) : (state.showModal = false);
    },

    setSelectionModal: (state, action: PayloadAction<boolean>) => {
      state.showModal = action.payload;
      state.object = undefined;
    },
  },
});

export default modelSlice.reducer;

export const { setSelection, setSelectionModal } = modelSlice.actions;

export const selectSelectionModal = (state: RootState): boolean => state.selection.showModal;
export const selectSelectedObject = (state: RootState): SelectedObject => state.selection.object;
