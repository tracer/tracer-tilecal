import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import type {
  ActiveCell,
  ActiveModel,
  ActiveModule,
  GeometryState,
  GeometryTreeSlice,
  RootState,
  TreeNode,
  UpdateNodePayloadAction,
} from "#/types/app.types";
import { isMobile } from "#/utils/isMobile";
import { GEOMETRY_MENU_TREE, GEOMETRY_MENU_TREE_DETAILED } from "#/constants/geometryTree";

const showGeometryMenu = isMobile() === false;

const initialState: GeometryTreeSlice = {
  tree: GEOMETRY_MENU_TREE,
  detailedTree: false,
  activeCells: updateActiveObjects(GEOMETRY_MENU_TREE).activeCells,
  activeModels: updateActiveObjects(GEOMETRY_MENU_TREE).activeModels,
  activeModules: [],
  showGeometryMenu,
};

export const geometrySlice = createSlice({
  name: "tree",
  initialState,
  reducers: {
    rehydrate: (state, action) => {
      return action.payload.tree || state;
    },
    updateParentNodeState: (state, action: UpdateNodePayloadAction) => {
      const { nodeId, propToChange, value, restrictAncestorsUpdate } = action.payload;

      if (restrictAncestorsUpdate) {
        const updatedTree = state.tree.map(
          (node: TreeNode): TreeNode => updateParentNode(node, nodeId, propToChange, value, false)
        );

        state.tree = updateNodeAndAncestors(updatedTree, nodeId, value as GeometryState);
      } else {
        const updatedTree = state.tree.map(
          (node: TreeNode): TreeNode => updateParentNode(node, nodeId, propToChange, value, true)
        );

        state.tree = updateNodeAndAncestors(updatedTree, nodeId, value as GeometryState);
      }
      const activeObjects = updateActiveObjects(state.tree);

      state.activeCells = activeObjects.activeCells;
      state.activeModules = activeObjects.activeModules;
      state.activeModels = activeObjects.activeModels;
    },
    updateChildNodeState: (state, action: UpdateNodePayloadAction) => {
      const { nodeId, propToChange, value } = action.payload;

      const updatedTree = state.tree.map(
        (node: TreeNode): TreeNode => updateChildNode(node, nodeId, propToChange, value)
      );

      state.tree = updateNodeAndAncestors(updatedTree, nodeId, value as GeometryState);

      const activeObjects = updateActiveObjects(state.tree);

      state.activeCells = activeObjects.activeCells;
      state.activeModules = activeObjects.activeModules;
      state.activeModels = activeObjects.activeModels;
    },
    setGeometryMenuVisibility: (state, action: PayloadAction<boolean>) => {
      state.showGeometryMenu = action.payload;
    },
    setDetailedTree: (state, action: PayloadAction<boolean>) => {
      state.detailedTree = action.payload;
      state.tree = action.payload ? GEOMETRY_MENU_TREE_DETAILED : GEOMETRY_MENU_TREE;
    },
  },
});

export default geometrySlice.reducer;

export const {
  updateChildNodeState,
  updateParentNodeState,
  setGeometryMenuVisibility,
  setDetailedTree,
} = geometrySlice.actions;
export const selectGeometryTree = (state: RootState): TreeNode[] => state.tree.tree;
export const selectActiveCells = (state: RootState): ActiveCell[] => state.tree.activeCells;
export const selectActiveModules = (state: RootState): ActiveModule[] => state.tree.activeModules;
export const selectActiveGeometries = (state: RootState): ActiveModel[] => state.tree.activeModels;
export const selectGeometryMenu = (state: RootState): boolean => state.tree.showGeometryMenu;
export const selectDetailedTree = (state: RootState): boolean => state.tree.detailedTree;

type UpdateNodeFunction = (
  node: TreeNode,
  nodeId: string,
  propToChange: string,
  value: string | boolean,
  updateDescendands?: boolean
) => TreeNode;

export function updateNodeAndAncestors(
  tree: TreeNode[],
  nodeId: string,
  modelState: GeometryState
): TreeNode[] {
  const updateNode = (node: TreeNode): TreeNode | null => {
    if (node.id === nodeId) {
      return { ...node, state: modelState };
    }

    if (node.children) {
      const updatedChildren = node.children.map(updateNode).filter(Boolean) as TreeNode[];

      if (updatedChildren.length > 0) {
        const newChildren = node.children.map((child) => {
          const updatedChild = updatedChildren.find((updated) => updated.id === child.id);
          return updatedChild ? updatedChild : child;
        });

        const childrenStates = newChildren.map((child) => child.state);
        const allLoaded = childrenStates.every((state) => state === "isLoaded");
        const someLoaded = childrenStates.some((state) => state === "isLoaded");
        const allNotLoaded = childrenStates.every((state) => state === "notLoaded");
        const somePartial = childrenStates.some((state) => state === "partialyLoaded");

        let updatedState = node.state;

        if (allLoaded) {
          updatedState = "isLoaded";
        } else if (someLoaded || somePartial) {
          updatedState = "partialyLoaded";
        } else if (allNotLoaded) {
          updatedState = "notLoaded";
        }

        return { ...node, state: updatedState, children: newChildren };
      }
    }

    return null;
  };

  return tree.map((node) => updateNode(node) || node);
}

const updateDescendandNodes = (
  node: TreeNode,
  propToChange: string,
  value: string | boolean
): TreeNode => {
  if (node.children) {
    return {
      ...node,
      [propToChange]: value,
      children: node.children.map((node) => updateDescendandNodes(node, propToChange, value)),
    };
  } else {
    return {
      ...node,
      [propToChange]: value,
    };
  }
};

const updateParentNode: UpdateNodeFunction = (
  node,
  nodeId,
  propToChange,
  modelState,
  updateDescendands
): TreeNode => {
  if (node.id === nodeId) {
    if (updateDescendands) {
      // Found the node with the matching ID, update its descendants
      const updatedNode = {
        ...node,
        [propToChange]: modelState,
        children: node.children
          ? node.children.map((node) => updateDescendandNodes(node, propToChange, modelState))
          : [],
      };
      return updatedNode;
    } else {
      // Found the node with the matching ID, update its descendants
      const updatedNode = {
        ...node,
        [propToChange]: modelState,
        children: node.children
          ? node.children.map((node) =>
              updateParentNode(node, nodeId, propToChange, modelState, updateDescendands)
            )
          : [],
      };
      return updatedNode;
    }
  } else if (node.children) {
    // Node does not have the matching ID, update its children
    return {
      ...node,
      children: node.children.map((node) =>
        updateParentNode(node, nodeId, propToChange, modelState, updateDescendands)
      ),
    };
  } else {
    // Node does not have children, return it without any changes
    return node;
  }
};

const updateChildNode: UpdateNodeFunction = (node, nodeId, propToChange, modelState): TreeNode => {
  if (node.children) {
    return {
      ...node,
      children: node.children.map((node) =>
        updateChildNode(node, nodeId, propToChange, modelState)
      ),
    };
  }

  if (node.id === nodeId) {
    return {
      ...node,
      [propToChange]: modelState,
    };
  }
  return node;
};

type UpdateActiveObjects = {
  activeCells: ActiveCell[];
  activeModules: ActiveModule[];
  activeModels: ActiveModel[];
};

export function updateActiveObjects(tree: TreeNode[]): UpdateActiveObjects {
  const activeCells: ActiveCell[] = [];
  const activeModules: ActiveModule[] = [];
  const activeModels: ActiveModel[] = [];

  function traverse(node: TreeNode): void {
    if (node.children) {
      node.children.forEach((child) => traverse(child));
    }

    if (node.state === "isLoaded") {
      if (node.modelPath) {
        activeModels.push({
          uid: node.id,
          name: node.name,
          modelPath: node.modelPath,
        });
      } else if (node.partition) {
        if (node.isModule) {
          activeModules.push({
            uid: node.id,
            name: node.name,
            partition: node.partition,
          });
        } else {
          activeCells.push({
            uid: node.id,
            name: node.name,
            partition: node.partition,
          });
        }
      }
    }
  }

  tree.forEach((node) => traverse(node));
  return {
    activeCells,
    activeModules,
    activeModels,
  };
}
