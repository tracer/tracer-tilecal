import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import type {
  CellColorMode,
  ParameterPerCellObject,
  ParameterPerChannelMinMax,
  ParameterPerChannelObject,
  RootState,
  TCal,
  VisualisationType,
} from "#/types/app.types";
import { ColorSchemes } from "#/types/app.types";
import {
  createParameterPerCellObject,
  createParameterPerChannelObject,
  fillNoisePerCellObject,
  fillParameterPerChannelObject,
} from "#/utils/visualisationParameterUtils";

const colorScheme1 = [
  "#5d5a6f",
  "#437898",
  "#3f859f",
  "#3a858b",
  "#44774c",
  "#a0963f",
  "#ba9b40",
  "#b78641",
  "#b84645",
];

const colorScheme2 = [
  "#30336b",
  "#4652b0",
  "#4385bb",
  "#75b670",
  "#badc58",
  "#f8c44e",
  "#f4a13b",
  "#f07c3f",
  "#eb4d4b",
];

const colorScheme3 = [
  "#341f97",
  "#3d49aa",
  "#3f6fbd",
  "#3696d0",
  "#0abde3",
  "#feae4f",
  "#fb914d",
  "#f5744f",
  "#ee5253",
];

const colorScheme4 = [
  "#ffbe0b",
  "#fb5607",
  "#ff006e",
  "#8338ec",
  "#3a86ff",
  "#00b4d8",
  "#4ea8de",
  "#577590",
  "#ee5253",
];

export const cellColorModes = [
  "total energies",
  "pmt1/pmt2 energies",
  "calibration constants per channel",
  "noise per cell",
] as const;

export const noisePerCellConstants = ["RMS", "pileup", "RMS1", "RMS2", "Ratio"] as const;

export const visualisationTypes = ["default", "parameter alongside eta"] as const;

const PI2 = +(2 * Math.PI).toFixed(1);

const initialState: TCal = {
  isSelected: false,
  cellInfo: null,
  colorSchemes: {
    default: colorScheme2,
    lowExposure: colorScheme1,
    colorblind: colorScheme3,
    customScheme: colorScheme4,
  },

  energyColorScheme: "default",
  energySchemeIntervals: [],
  energyLimits: {
    min: 0,
    max: 0,
  },
  filterCellColorsMinMax: {
    min: undefined,
    max: undefined,
  },
  angleFilter: {
    theta: {
      defaultValues: [0, PI2],
      values: [0, PI2],
    },
    phi: {
      defaultValues: [0, PI2],
      values: [0, PI2],
    },
    eta: {
      defaultValues: [-1.5, 1.5],
      values: [-1.5, 1.5],
    },
  },
  cellColorMode: "total energies",
  fixScale: false,
  visualisationType: "default",

  parameterPerChannel: createParameterPerChannelObject(),
  parameterPerCell: createParameterPerCellObject(),
  parameterPerChannelMinMax: { 0: {}, 1: {} },
  selectedConstant: "0",
  selectedGain: "0",
  selectedUploadedFile: 0,
  uploadedFiles: null,
};

export const tCalSlice = createSlice({
  name: "tcal",
  initialState,
  reducers: {
    rehydrate: (state, action) => {
      return action.payload.cell || state;
    },
    setCellIsSelected: (state, action: PayloadAction<TCal>) => {
      state.isSelected = action.payload.isSelected;
      state.cellInfo = action.payload.cellInfo;
    },
    setEnergyLimits: (state, action: PayloadAction<TCal["energyLimits"]>) => {
      if (!state.fixScale) {
        state.energyLimits = action.payload;

        const { max, min } = action.payload;
        const eighth = Math.abs(min - max) / 8;
        const intervals = [];

        for (let k = 9; k > 0; k--) {
          const cellColor = state.colorSchemes[state.energyColorScheme][k - 1];
          const value = (max - (9 - k) * eighth).toFixed(2);
          intervals.push({ value, cellColor });
        }

        state.energySchemeIntervals = intervals;
      }
    },
    setAngleFilterThetaLimits: (state, action: PayloadAction<TCal["angleFilter"]["theta"]>) => {
      state.angleFilter.theta = action.payload;
    },
    setAngleFilterPhiLimits: (state, action: PayloadAction<TCal["angleFilter"]["phi"]>) => {
      state.angleFilter.phi = action.payload;
    },
    setAngleFilterEtaLimits: (state, action: PayloadAction<TCal["angleFilter"]["eta"]>) => {
      state.angleFilter.eta = action.payload;
    },
    setFilterCellColorsMinMax: (state, action: PayloadAction<TCal["filterCellColorsMinMax"]>) => {
      state.filterCellColorsMinMax = action.payload;
    },
    setCellColorMode: (state, action: PayloadAction<CellColorMode>) => {
      state.cellColorMode = action.payload;
    },
    setFixScaleMode: (state, action: PayloadAction<boolean>) => {
      state.fixScale = action.payload;
    },
    setEnergyColorScheme: (state, action: PayloadAction<keyof ColorSchemes>) => {
      state.energyColorScheme = action.payload;
    },
    setCustomColorScheme: (state, action: PayloadAction<string[]>) => {
      state.colorSchemes.customScheme = action.payload;
    },
    setSelectedConstant: (state, action: PayloadAction<string>) => {
      state.selectedConstant = action.payload;
    },
    setSelectedGain: (state, action: PayloadAction<string>) => {
      state.selectedGain = action.payload;
    },
    setVisualisationType: (state, action: PayloadAction<VisualisationType>) => {
      state.visualisationType = action.payload;
    },
    setSelectedUploadedFile: (state, action: PayloadAction<number>) => {
      state.selectedUploadedFile = action.payload;
    },
    setUploadedFiles: (state, action: PayloadAction<FileList | null>) => {
      state.uploadedFiles = action.payload;
    },
    setParameterPerChannelFileData: (state, action: PayloadAction<string>) => {
      const { parameterPerChannel, parameterPerChannelMinMax } = fillParameterPerChannelObject(
        action.payload
      );
      state.parameterPerChannel = parameterPerChannel;
      state.parameterPerChannelMinMax = parameterPerChannelMinMax;
    },
    setNoisePerCellFileData: (state, action: PayloadAction<string>) => {
      const { parameterPerCell, parameterPerCellMinMax } = fillNoisePerCellObject(action.payload);
      state.parameterPerCell = parameterPerCell;
      state.parameterPerChannelMinMax = parameterPerCellMinMax;
    },
    setEnergySchemeIntervalMinMax: (state, action: PayloadAction<{ min: string; max: string }>) => {
      const { min, max } = action.payload;
      const tempArray = state.energySchemeIntervals;

      const minInterval = tempArray[tempArray.length - 1];
      const maxInterval = tempArray[0];

      minInterval.value = min;
      maxInterval.value = max;

      const eighth = Math.abs(parseFloat(min) - parseFloat(max)) / 8;
      const intervals = [];

      for (let k = 9; k > 0; k--) {
        const cellColor = state.colorSchemes[state.energyColorScheme][k - 1];
        const value = (parseFloat(max) - (9 - k) * eighth).toFixed(2);
        intervals.push({ value, cellColor });
      }

      intervals.shift();
      intervals.pop();

      state.energySchemeIntervals = [maxInterval, ...intervals, minInterval];
    },
  },
});

export default tCalSlice.reducer;

export const {
  setCellIsSelected,
  setFilterCellColorsMinMax,
  setEnergyLimits,
  setAngleFilterThetaLimits,
  setAngleFilterPhiLimits,
  setEnergySchemeIntervalMinMax,
  setCellColorMode,
  setFixScaleMode,
  setEnergyColorScheme,
  setCustomColorScheme,
  setAngleFilterEtaLimits,
  setParameterPerChannelFileData,
  setSelectedConstant,
  setVisualisationType,
  setSelectedGain,
  setNoisePerCellFileData,
  setSelectedUploadedFile,
  setUploadedFiles,
} = tCalSlice.actions;

export const selectCellInfo = (state: RootState): TCal => state.tcal;
export const selectEnergyLimits = (state: RootState): TCal["energyLimits"] =>
  state.tcal.energyLimits;
export const selectEnergyColorScheme = (state: RootState): keyof ColorSchemes =>
  state.tcal.energyColorScheme;
export const selectThetaAngleFilterLimits = (state: RootState): TCal["angleFilter"]["theta"] =>
  state.tcal.angleFilter.theta;
export const selectPhiAngleFilterLimits = (state: RootState): TCal["angleFilter"]["phi"] =>
  state.tcal.angleFilter.phi;
export const selectEtaAngleFilterLimits = (state: RootState): TCal["angleFilter"]["eta"] =>
  state.tcal.angleFilter.eta;
export const selectFilterCellColorsMinMax = (state: RootState): TCal["filterCellColorsMinMax"] =>
  state.tcal.filterCellColorsMinMax;
export const selectCellColorMode = (state: RootState): CellColorMode => state.tcal.cellColorMode;
export const selectEnergySchemeIntervals = (state: RootState): TCal["energySchemeIntervals"] =>
  state.tcal.energySchemeIntervals;
export const selectFixScale = (state: RootState): boolean => state.tcal.fixScale;
export const selectColorSchemes = (state: RootState): ColorSchemes => state.tcal.colorSchemes;
export const selectParameterPerChannel = (state: RootState): ParameterPerChannelObject =>
  state.tcal.parameterPerChannel;
export const selectParameterPerChannelMinMax = (state: RootState): ParameterPerChannelMinMax =>
  state.tcal.parameterPerChannelMinMax;
export const selectParameterPerCell = (state: RootState): ParameterPerCellObject =>
  state.tcal.parameterPerCell;
export const selectSelectedConstant = (state: RootState): string => state.tcal.selectedConstant;
export const selectSelectedGain = (state: RootState): string => state.tcal.selectedGain;
export const selectVisualisationType = (state: RootState): VisualisationType =>
  state.tcal.visualisationType;
export const selectSelectedUploadedFile = (state: RootState): number =>
  state.tcal.selectedUploadedFile;
export const selectUploadedFiles = (state: RootState): FileList | null => state.tcal.uploadedFiles;
