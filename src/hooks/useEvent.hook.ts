import MetModel from "#/models//Met";
import Channels from "#/models/Channels";
import JetModel from "#/models/Jet";
import TileModel from "#/models/Tile";
import TileModule from "#/models/TileModule";
import TrackModel from "#/models/Track";

import { useMemo } from "react";

import type { EventDetailsXML, EventNumber, EventsToShow, TrackFilter } from "#/types/app.types";
import {
  selectEvent,
  selectEventNumber,
  selectEventParameters,
  selectTrackFilter,
} from "#/store/features/eventSlice";
import { useAppSelector } from "#/store/hooks";

const jetService = new JetModel();
const trackService = new TrackModel();
const metModel = new MetModel();
const tileModel = new TileModel(Channels);
const tileModule = new TileModule();

export default function useEvent(): useEvent {
  const event = useAppSelector(selectEvent);
  const eventParameters = useAppSelector(selectEventParameters);
  const trackFilterValues = useAppSelector(selectTrackFilter);
  const eventNumber = useAppSelector(selectEventNumber);

  const payload = useMemo(
    (): useEvent => ({
      event,
      eventNumber,
      eventParameters,
      trackFilterValues,
      JET: jetService,
      MET: metModel,
      TRACK: trackService,
      TILE: tileModel,
      TileModule: tileModule,
    }),
    [event, eventParameters, trackFilterValues, eventNumber]
  );
  return payload;
}

interface useEvent {
  event: EventDetailsXML | null;
  eventParameters: EventsToShow;
  trackFilterValues: TrackFilter;
  eventNumber: EventNumber;
  JET: JetModel;
  MET: MetModel;
  TRACK: TrackModel;
  TILE: TileModel;
  TileModule: TileModule;
}
