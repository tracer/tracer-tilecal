import { useEffect } from "react";

type KeyCallback = (event: KeyboardEvent) => void;

export default function useKeydown(
  keys: string | string[],
  callback: KeyCallback
): void {
  useEffect(() => {
    const handleKeydown = (event: KeyboardEvent): void => {
      const keyArray = Array.isArray(keys) ? keys : [keys];
      if (keyArray.includes(event.key)) {
        callback(event);
      }
    };

    window.addEventListener("keydown", handleKeydown);

    return () => {
      window.removeEventListener("keydown", handleKeydown);
    };
  }, [keys, callback]);
}
