import { uid } from "#/utils/uid";
import { CAVERN } from "#/constants/tree/detector/cavern/cavern";
import { MAIN_COMPONENTS } from "#/constants/tree/detector/main-components/main-components";
import { MAIN_COMPONENTS_DETAILED } from "#/constants/tree/detector/main-components/main-components-detailed";
import { SUPPORT_STRUCTURE } from "#/constants/tree/detector/support-structure/supportStructure";

export type GeometryState = "notLoaded" | "partialyLoaded" | "isLoaded";

export interface TreeNode {
  id: string;
  name: string;
  state: GeometryState;
  modelPath?: string;
  showChildren?: boolean;
  children?: TreeNode[];
  root?: boolean;
  nodeEnd?: boolean;
  renderOrder?: number;
}

export const GEOMETRY_MENU_TREE: TreeNode[] = [
  {
    id: uid(),
    name: "ATLAS Detector",
    state: "partialyLoaded",
    showChildren: true,
    root: true,
    children: [MAIN_COMPONENTS],
  },
];

export const GEOMETRY_MENU_TREE_DETAILED: TreeNode[] = [
  {
    id: uid(),
    name: "ATLAS Detector",
    state: "partialyLoaded",
    showChildren: true,
    root: true,
    children: [MAIN_COMPONENTS_DETAILED, SUPPORT_STRUCTURE, CAVERN],
  },
];
