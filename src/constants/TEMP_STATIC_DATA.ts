import type { EmployeeStaticData } from "#/types/app.types";

export const TEMP_INFO: EmployeeStaticData = {
  employees: [
    {
      id: "2",
      name: "Sharmazanashvili Alexander",
      src: "/assets/images/sharmazanashvili-1.jpg",
      role: "Project manager, Concept creator",
      contact: "http://cadcam.ge/sharmazanashvili-personal/",
    },

    {
      id: "5i",
      name: "Kverenchkhiladze Irakli",
      src: "/assets/images/kverenchxiladze.png",
      role: "Chief Software developer",
      contact: "http://cadcam.ge/kverenchkhiladze-personal/",
    },
    {
      id: "4",
      name: "Zurashvili Nino",
      src: "/assets/images/niko-1.png",
      role: "Software developer",
      contact: "http://cadcam.ge/zurashvili-personal/",
    },

    {
      id: "6",
      name: "Alikhanovi Alexander",
      src: "/assets/images/alixanovi.png",
      role: "Geometry designer",
      contact: "http://cadcam.ge/alikhanovi-personal/",
    },
    {
      id: "3",
      name: "Kekelia Besik",
      src: "/assets/images/kekelia-3.png",
      role: "Geometry designer",
      contact: "http://cadcam.ge/kekelia-personal/",
    },

    {
      id: "7",
      name: "Mirziashvili Giorgi",
      src: "/assets/images/Mirziashvili-1.png",
      role: "Geometry designer",
      contact: "http://cadcam.ge/mirziashvili-personal/",
    },

    {
      id: "9",
      name: "Tsutskiridze Kote",
      src: "/assets/images/kekelia-3-1.png",
      role: "Geometry designer",
      contact: "http://cadcam.ge/page/",
    },
    {
      id: "91",
      name: "Kobakhidze Shota",
      src: "/assets/images/kekelia-3-1.png",
      role: "Geometry designer",
      contact: "http://cadcam.ge/page/",
    },
    {
      id: "91-e",
      name: "Udzilauri Nikoloz",
      src: "/assets/images/kekelia-3-1.png",
      role: "UI Concept designer",
      contact: "http://cadcam.ge/page/",
    },
  ],
};
