import type { CellProps } from "#/types/app.types";

export const cellProps: CellProps = {
  A: {
    r: 2.45,
    h: 0.3,
    d: [
      0.24648, 0.24648, 0.2556, 0.25561, 0.27386, 0.283, 0.29212, 0.31951, 0.33776,
      0.29212, 0, 0.16428, 0.45634, 0.5111, 0.54758, 0.87617,
    ],
    z: [],
    w: [0, 0],
  },
  BLB: {
    r: 2.795,
    h: 0.39,
    d: [
      0.28299, 0.283, 0.28299, 0.30125, 0.30125, 0.32864, 0.33776, 0.36516, 0.31954,
      0, 0.29206, 0.49284, 0.54761, 0.58412, 0.63887,
    ],
    w: [0, 0],
    z: [],
  },
  C: {
    r: 3.215,
    h: 0.45,
    d: [
      0.31951, 0.32864, 0.32863, 0.3469, 0.34689, 0.37428, 0.39254, 0.36515, 0,
      0.0947,
    ],
    z: [],
    w: [0, 0],
  },
  DLBD4: {
    r: 3.63,
    h: 0.38,
    d: [0.7303, 0.73944, 0.78507, 0.91288, 0.309, 1.18648, 1.36902],
    w: [0, 0],
    z: [],
  },
  BEB: { r: 2.87, h: 0.54, d: [], w: [0, 0], z: [] },
  DEB: { r: 3.48, h: 0.68, d: [], w: [0, 0], z: [] },
  E: { r: 0, h: 0, d: [0.015, 0.008], z: [3.552, 3.54], w: [0, 0] },
  E1: { r: 2.8025, h: 0.313, d: [], w: [0, 0], z: [] },
  E2: { r: 2.475, h: 0.341, d: [], w: [0, 0], z: [] },
  E3: { r: 2.066, h: 0.478, d: [], w: [0, 0], z: [] },
  E4: { r: 1.646, h: 0.362, d: [], w: [0, 0], z: [] },
  B: {
    r: 0,
    h: 0,
    d: [
      0.28299, 0.283, 0.28299, 0.30125, 0.30125, 0.32864, 0.33776, 0.36516, 0.31954,
      0, 0.29206, 0.49284, 0.54761, 0.58412, 0.63887,
    ],
    w: [0, 0],
    z: [],
  },
  D: {
    r: 0,
    h: 0,
    d: [0.7303, 0.73944, 0.78507, 0.91288, 0.309, 1.18648, 1.36902],
    w: [0, 0],
    z: [],
  },
};

export const faceCoordinatesHelper = [
  [
    //layer: 0
    [
      2.3, 2.6, 0, 0.24648, 0.49296, 0.74856, 1.00417, 1.27803, 1.56103, 1.85315,
      2.17266, 2.51042, 2.80254,
    ],
    //layer: 1
    [
      //sub-layer: B
      [
        2.6, 2.99, 0, 0.28299, 0.56599, 0.84898, 1.15023, 1.145148, 1.78012, 2.11788,
        2.48304, 2.80254,
      ],
      //sub-layer: C
      [
        2.99, 3.44, 0, 0.31951, 0.64815, 0.97678, 1.32368, 1.67057, 2.04487, 2.43739,
        2.80254,
      ],
    ],
    //layer2
    [3.44, 3.82, 0, 0.36515, 1.10459, 1.88966, 2.80254],
  ],
  //section: 2
  [
    //layer: 0
    [2.3, 2.6, 3.5745, 3.73878, 4.19512, 4.70622, 5.25383, 6.13],
    //layer: 1
    [2.6, 3.14, 3.5745, 3.86656, 4.3594, 4.90701, 5.49113, 6.13],
    //layer: 2 - sub-layer: B
    [3.14, 3.82, 3.5745, 4.76, 6.13],
  ],
  //section: 3
  [
    //layer: 1
    [2.99, 3.44, 3.46449, 3.5592],
    //layer: 2 - sub-layer: C
    [3.44, 3.82, 3.25, 3.5595],
    //layer: 4
    [
      //sub-layer E1
      [1.465, 1.827, 3.532, 3.54],
      //sub-layer E2
      [1.827, 2.305, 3.532, 3.54],
      //sub-layer E3
      [2.305, 2.646, 3.544, 3.5595],
      //sub-layer E4
      [2.646, 2.959, 3.544, 3.5595],
    ],
  ],
];
