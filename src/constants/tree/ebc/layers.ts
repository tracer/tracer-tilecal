import type { TreeNode } from "#/types/app.types";
import { uid } from "#/utils/uid";

export const LAYERS: TreeNode = {
  id: uid(),
  name: "Layers",
  state: "notLoaded",
  showChildren: true,
  nodeEnd: true,
  children: [
    {
      id: uid(),
      name: "a",
      state: "notLoaded",
      partition: "EBC",
    },
    {
      id: uid(),
      name: "bc",
      state: "notLoaded",
      partition: "EBC",
    },
    {
      id: uid(),
      name: "d",
      state: "notLoaded",
      partition: "EBC",
    },
    {
      id: uid(),
      name: "e",
      state: "notLoaded",
      nodeEnd: true,
      partition: "EBC",
    },
  ],
};
