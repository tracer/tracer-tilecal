import type { TreeNode } from "#/types/app.types";
import { uid } from "#/utils/uid";
import { LAYERS } from "#/constants/tree/ebc/layers";
import { MODULE } from "#/constants/tree/ebc/module";

export const EBC: TreeNode = {
  id: uid(),
  name: "ebc",
  state: "notLoaded",
  showChildren: false,
  nodeEnd: true,
  children: [MODULE, LAYERS],
};
