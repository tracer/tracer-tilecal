import { uid } from "#/utils/uid";
import { TreeNode } from "#/constants/geometryTree";
import { BEAM_PIPE } from "#/constants/tree/detector/main-components/beam-pipe";
import { CALORIMETRY } from "#/constants/tree/detector/main-components/calorimetry";
import { FORWARD_SHIELDING } from "#/constants/tree/detector/main-components/forward-shielding";
import { INNER_DETECTOR } from "#/constants/tree/detector/main-components/innerDetector";
import { ITK } from "#/constants/tree/detector/main-components/itk";
import { MAGNET_SYSTEMS } from "#/constants/tree/detector/main-components/magnetSystems";
import { MUON_SPECTROMETER_DETAILED } from "#/constants/tree/detector/main-components/muon-spectrometer-detailed";
import { PLATFORMS } from "#/constants/tree/detector/main-components/platforms";
import { SERVICES } from "#/constants/tree/detector/main-components/services";

export const MAIN_COMPONENTS_DETAILED: TreeNode = {
  id: uid(),
  name: "main components",
  state: "partialyLoaded",
  showChildren: true,
  children: [
    MAGNET_SYSTEMS,
    INNER_DETECTOR,
    ITK,
    CALORIMETRY,
    MUON_SPECTROMETER_DETAILED,
    FORWARD_SHIELDING,
    SERVICES,
    PLATFORMS,
    BEAM_PIPE,
  ],
};
