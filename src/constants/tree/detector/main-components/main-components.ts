import { uid } from "#/utils/uid";
import { TreeNode } from "#/constants/geometryTree";
import { CALORIMETRY } from "#/constants/tree/detector/main-components/calorimetry";
import { INNER_DETECTOR } from "#/constants/tree/detector/main-components/innerDetector";
import { MUON_SPECTROMETER } from "#/constants/tree/detector/main-components/muon-spectrometer";

export const MAIN_COMPONENTS: TreeNode = {
  id: uid(),
  name: "main components",
  state: "partialyLoaded",
  showChildren: true,
  nodeEnd: true,
  children: [INNER_DETECTOR, CALORIMETRY, MUON_SPECTROMETER],
};
