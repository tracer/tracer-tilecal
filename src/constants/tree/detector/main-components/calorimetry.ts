import { uid } from "#/utils/uid";
import { TreeNode } from "#/constants/geometryTree";
import { PARTITION } from "#/constants/tree/partition";

export const CALORIMETRY: TreeNode = {
  id: uid(),
  name: "calorimetry",
  state: "partialyLoaded",
  showChildren: true,
  children: [
    {
      id: uid(),
      name: "lar",
      state: "notLoaded",
      showChildren: false,
      children: [
        {
          id: uid(),
          name: "barrel",
          state: "notLoaded",
          modelPath: "core/main-components/calorimetry/lar/barrel/lar-barrel",
        },
        {
          id: uid(),
          name: "endcap",
          state: "notLoaded",
          showChildren: false,
          nodeEnd: true,
          children: [
            {
              id: uid(),
              name: "side a",
              state: "notLoaded",
              showChildren: false,
              children: [
                {
                  id: uid(),
                  name: "lar emec",
                  state: "notLoaded",
                  modelPath:
                    "core/main-components/calorimetry/lar/endcap/side-a/lar-emec/lar-emec-sidea",
                },
                {
                  id: uid(),
                  name: "lar hec",
                  state: "notLoaded",
                  modelPath:
                    "core/main-components/calorimetry/lar/endcap/side-a/lar-hec/lar-hec-sidea",
                },
                {
                  id: uid(),
                  name: "lar fcal",
                  state: "notLoaded",
                  modelPath:
                    "core/main-components/calorimetry/lar/endcap/side-a/lar-fcal/lar-fcal-sidea",
                  nodeEnd: true,
                },
              ],
            },
            {
              id: uid(),
              name: "side c",
              state: "notLoaded",
              showChildren: false,
              nodeEnd: true,
              children: [
                {
                  id: uid(),
                  name: "lar emec",
                  state: "notLoaded",
                  modelPath:
                    "core/main-components/calorimetry/lar/endcap/side-c/lar-emec/lar-emec-sidec",
                },
                {
                  id: uid(),
                  name: "lar hec",
                  state: "notLoaded",
                  modelPath:
                    "core/main-components/calorimetry/lar/endcap/side-c/lar-hec/lar-hec-sidec",
                },
                {
                  id: uid(),
                  name: "lar fcal",
                  state: "notLoaded",
                  modelPath:
                    "core/main-components/calorimetry/lar/endcap/side-c/lar-fcal/lar-fcal-sidec",
                  nodeEnd: true,
                },
              ],
            },
          ],
        },
      ],
    },
    {
      id: uid(),
      name: "Tile Calorimeter",
      state: "partialyLoaded",
      showChildren: true,
      nodeEnd: true,
      children: [PARTITION],
    },
  ],
};
