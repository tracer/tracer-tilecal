import { uid } from "#/utils/uid";
import { TreeNode } from "#/constants/geometryTree";

export const INNER_DETECTOR: TreeNode = {
  id: uid(),
  name: "inner detector",
  state: "notLoaded",
  showChildren: false,
  children: [
    {
      id: uid(),
      name: "pixel",
      state: "notLoaded",
      modelPath: "core/main-components/inner-detector/pixel/pixel",
      renderOrder: 1,
    },
    {
      id: uid(),
      name: "sct",
      state: "notLoaded",
      showChildren: false,
      children: [
        {
          id: uid(),
          name: "barrel",
          state: "notLoaded",
          modelPath: "core/main-components/inner-detector/sct/barrel/sct-bar",
          renderOrder: 1,
        },
        {
          id: uid(),
          name: "endcap",
          state: "notLoaded",
          showChildren: false,
          children: [
            {
              id: uid(),
              name: "side a",
              state: "notLoaded",
              modelPath: "core/main-components/inner-detector/sct/endcap/side-a/sct-sidea",
              renderOrder: 1,
            },
            {
              id: uid(),
              name: "side c",
              state: "notLoaded",
              modelPath: "core/main-components/inner-detector/sct/endcap/side-c/sct-sidec",
              renderOrder: 1,
              nodeEnd: true,
            },
          ],
        },
      ],
    },
    {
      id: uid(),
      name: "trt",
      state: "notLoaded",
      showChildren: false,
      nodeEnd: true,
      children: [
        {
          id: uid(),
          name: "barrel",
          state: "notLoaded",
          modelPath: "core/main-components/inner-detector/trt/barrel/trt-bar",
          renderOrder: 1,
        },
        {
          id: uid(),
          name: "endcap",
          state: "notLoaded",
          showChildren: false,
          nodeEnd: true,
          children: [
            {
              id: uid(),
              name: "side a",
              state: "notLoaded",
              modelPath: "core/main-components/inner-detector/trt/endcap/side-a/trt-sidea",
              renderOrder: 1,
            },
            {
              id: uid(),
              name: "side c",
              state: "notLoaded",
              modelPath: "core/main-components/inner-detector/trt/endcap/side-c/trt-sidec",
              renderOrder: 1,
              nodeEnd: true,
            },
          ],
        },
      ],
    },
  ],
};
