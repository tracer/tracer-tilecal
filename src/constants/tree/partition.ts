import type { TreeNode } from "#/types/app.types";
import { uid } from "#/utils/uid";
import { EBA } from "#/constants/tree/eba/eba";
import { EBC } from "#/constants/tree/ebc/ebc";
import { LBA } from "#/constants/tree/lba/lba";
import { LBC } from "#/constants/tree/lbc/lbc";

export const PARTITION: TreeNode = {
  id: uid(),
  name: "partition",
  state: "partialyLoaded",
  showChildren: true,
  nodeEnd: true,
  children: [LBA, LBC, EBA, EBC],
};
