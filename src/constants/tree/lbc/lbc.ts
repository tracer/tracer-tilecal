import type { TreeNode } from "#/types/app.types";
import { uid } from "#/utils/uid";
import { LAYERS } from "#/constants/tree/lbc/layers";
import { MODULE } from "#/constants/tree/lbc/module";

export const LBC: TreeNode = {
  id: uid(),
  name: "lbc",
  state: "notLoaded",
  showChildren: false,
  children: [MODULE, LAYERS],
};
