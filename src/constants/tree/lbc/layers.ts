import type { TreeNode } from "#/types/app.types";
import { uid } from "#/utils/uid";

export const LAYERS: TreeNode = {
  id: uid(),
  name: "Layers",
  state: "notLoaded",
  showChildren: true,
  children: [
    {
      id: uid(),
      name: "a",
      state: "notLoaded",
      partition: "LBC",
    },
    {
      id: uid(),
      name: "bc",
      state: "notLoaded",
      partition: "LBC",
    },
    {
      id: uid(),
      name: "d",
      state: "notLoaded",
      partition: "LBC",
      nodeEnd: true,
    },
  ],
};
