import type { TreeNode } from "#/types/app.types";
import { uid } from "#/utils/uid";
import { LAYERS } from "#/constants/tree/eba/layers";
import { MODULE } from "#/constants/tree/eba/module";

export const EBA: TreeNode = {
  id: uid(),
  name: "eba",
  state: "notLoaded",
  showChildren: false,
  children: [MODULE, LAYERS],
};
