import type { TreeNode } from "#/types/app.types";
import { setModule } from "#/utils/setModules";
import { uid } from "#/utils/uid";

export const MODULE: TreeNode = {
  id: uid(),
  name: "module",
  state: "notLoaded",
  showChildren: false,
  children: setModule("EBA"),
};
