import type { TreeNode } from "#/types/app.types";
import { uid } from "#/utils/uid";

export const LAYERS: TreeNode = {
  id: uid(),
  name: "Layers",
  state: "isLoaded",
  showChildren: true,
  children: [
    {
      id: uid(),
      name: "a",
      state: "isLoaded",
      partition: "LBA",
    },
    {
      id: uid(),
      name: "bc",
      state: "isLoaded",
      partition: "LBA",
    },
    {
      id: uid(),
      name: "d",
      state: "isLoaded",
      nodeEnd: true,
      partition: "LBA",
    },
  ],
};
