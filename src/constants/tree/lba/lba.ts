import type { TreeNode } from "#/types/app.types";
import { uid } from "#/utils/uid";
import { LAYERS } from "#/constants/tree/lba/layers";
import { MODULE } from "#/constants/tree/lba/module";

export const LBA: TreeNode = {
  id: uid(),
  name: "lba",
  state: "partialyLoaded",
  showChildren: false,
  children: [MODULE, LAYERS],
};
