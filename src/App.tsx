import { Stats as Stat } from "@react-three/drei";
import { lazy, Suspense } from "react";
import { Toaster } from "react-hot-toast";

import FisheyeMode from "#/components/FisheyeMode.component";
import AngleFilterModal from "#/components/modals/angleFilter/angleFilter";
import ModelInformation from "#/components/modals/modelInformation/ModelInformation.component";
import SelectedObjectInformation from "#/components/modals/selectedObjectInformation/SelectedObjectInformation";
import Navigation from "#/components/navigation/index";
import VideoPulse from "#/components/ScreenRecording.component";
import ColorScheme from "#/components/three/ColorScheme";
import FlyOverlay from "#/components/three/FlyGraph.component";
import FlyStats from "#/components/three/FlyStats.component";
import Stats from "#/components/three/Stats.component";
import Tree from "#/components/tree/tree/Tree.component";

const Canvas = lazy(() => import("#/three/canvas.three"));
const Logo = lazy(() => import("#/components/Logo.component"));

export default function App() {
  return (
    <>
      <Navigation />
      <main className="absolute left-0 top-0 z-10 h-screen w-screen">
        <Suspense fallback={null}>
          <Canvas />
          <Logo />
        </Suspense>
      </main>
      <SelectedObjectInformation />
      <ModelInformation />
      <AngleFilterModal />
      <Tree />
      <FisheyeMode />
      <ColorScheme />
      <Stats />
      <FlyOverlay />
      <FlyStats />
      <VideoPulse />
      <Toaster position="top-right" />
      <Stat />
    </>
  );
}
