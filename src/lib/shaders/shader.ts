export const vertexShader = `
  varying vec4 pos;

  void main() {
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
    pos = vec4(position, 1.0);
  }
`;
export const fragmentShader = `
  uniform vec3 color1;
  uniform vec3 color2;
  uniform vec3 edge_color;
  varying vec4 pos; 
  uniform float depth;
  uniform float height;
  uniform float width1;
  uniform float width2;
  uniform float opacity;
  uniform float edgeGap;
  uniform int type;


  void main() {
    float n = (  height - pos.y) / (2.0 * height);
    // float pct = float(abs(abs(pos.z-depth) - abs(depth)) < edgeGap) + float(abs(abs(pos.y) - abs(height)) < edgeGap) + float((width2 - width1) * n + width1 - abs(pos.x) < edgeGap);
    float checkDepth = float(abs(abs(pos.z-depth) - abs(depth)) < edgeGap);

    float checkWidth;
    if (type == 1){
      checkWidth = float((width2 - width1) * n + width1 - abs(pos.x) < edgeGap);
    } else{
      checkWidth = float((width2 - width1) * n + width1 - abs(pos.x) < edgeGap);
    }

    float checkHeight = float(abs(abs(pos.y) - abs(height)) < edgeGap);


    float pct;
    
    pct = checkDepth + checkWidth + checkHeight;
    pct=float(pct>=2.0 );

    
    vec3 color;
    
    if ( pos.x / (width2-width1) < 0.5) { 
      color=color1;
    } else {
      color=color2;
    }
    
    gl_FragColor = vec4(mix(color.rgb, edge_color.rgb, pct), opacity);
  }
`;
