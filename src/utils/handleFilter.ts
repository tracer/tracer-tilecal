import { MeshType } from "#/models/Tile";
import { Vector3 } from "three";

import { TCal } from "#/types/app.types";

type Config = {
  mesh: MeshType;
  colorFilter: TCal["filterCellColorsMinMax"];
  thetaFilter: TCal["angleFilter"]["theta"]["values"];
  phiFilter: TCal["angleFilter"]["phi"]["values"];
  etaFilter: TCal["angleFilter"]["eta"]["values"];
};

const setphiFromPosition = (position: Vector3): number => {
  const phi = Math.atan((-1 * position.y) / position.x);
  if (phi < 0) {
    if (position.y > 0) return phi + Math.PI;
    else return phi + 2 * Math.PI;
  } else if (position.y < 0) return phi + Math.PI;
  else return phi;
};

const setThetaFromPosition = (position: Vector3): number => {
  const theta = Math.atan((-1 * position.y) / position.z);
  if (theta < 0) {
    if (position.y > 0) return theta + Math.PI;
    else return theta + 2 * Math.PI;
  } else if (position.y < 0) return theta + Math.PI;
  else return theta;
};

export const handleFilter = (config: Config): void => {
  const { mesh, colorFilter, thetaFilter, phiFilter, etaFilter } = config;
  const theta = setThetaFromPosition(mesh.userData.position);
  const phi = setphiFromPosition(mesh.userData.position);
  const eta = mesh.userData.eta;

  const thetaCheck = theta < thetaFilter[0] || theta > thetaFilter[1];
  const phiCheck = phi < phiFilter[0] || phi > phiFilter[1];
  const etaCheck = eta < etaFilter[0] || eta > etaFilter[1];

  const visualisationParam =
    mesh.userData.energy ||
    mesh.userData["calibration constants"] || [
      mesh.userData.pmt1Energy,
      mesh.userData.pmt2Energy,
    ] ||
    mesh.userData.noiseConstant[1];
  const colorCheck =
    (colorFilter.min !== undefined &&
      !(
        visualisationParam > colorFilter.min ||
        visualisationParam[0] > colorFilter.min ||
        visualisationParam[1] > colorFilter.min
      )) ||
    (colorFilter.max !== undefined &&
      !(
        visualisationParam < colorFilter.max ||
        visualisationParam[0] < colorFilter.max ||
        visualisationParam[1] < colorFilter.max
      ));

  if (thetaCheck || colorCheck || phiCheck || etaCheck) {
    mesh.material.visible = false;
  } else {
    mesh.material.visible = true;
  }
};
