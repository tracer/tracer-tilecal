import { DisabledEventObject } from "#/types/app.types";

interface ValidateEvent {
  Event: any;
}

type EventObject = {
  isValid: boolean;
  disabledEventObjects: DisabledEventObject[];
};

export function validateEventFile(event: ValidateEvent): EventObject {
  const eventObjects: EventObject = {
    isValid: false,
    disabledEventObjects: [],
  };

  if (!event.Event) {
    eventObjects.isValid = false;
    return eventObjects;
  }

  if (!event.Event.Jet) {
    eventObjects.disabledEventObjects.push("jet");
  }

  if (!event.Event.ETMis) {
    eventObjects.disabledEventObjects.push("etmis");
  }

  if (!event.Event.Track) {
    eventObjects.disabledEventObjects.push("track");
  }

  eventObjects.isValid = true;

  return eventObjects;
}
