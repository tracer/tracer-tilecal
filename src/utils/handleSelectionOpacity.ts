import type { MeshType } from "#/models/Tile";

import type { SelectedObject } from "#/store/features/selection";

export const handleSelectionOpacity = (selectedObject: SelectedObject, mesh: MeshType): void => {
  if (selectedObject && mesh.userData.id !== selectedObject.id) {
    mesh.material.uniforms.opacity.value = 0.2;
  } else {
    mesh.material.uniforms.opacity.value = 1;
  }
};
