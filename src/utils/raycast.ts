import { Raycaster, Scene, Vector2 } from "three";

import { Camera } from "@react-three/fiber";

import type { SelectedObject } from "#/store/features/selection";

type Ev = { clientX: number; clientY: number };

interface Props {
  mouse: Vector2;
  e: Ev;
  width: number;
  height: number;
  camera: Camera;
  scene: Scene;
  raycaster: Raycaster;
  many: boolean;
}

export function raycast({
  mouse,
  e,
  width,
  height,
  camera,
  scene,
  raycaster,
  many,
}: Props): SelectedObject {
  mouse.x = (e.clientX / width) * 2 - 1;
  mouse.y = -(e.clientY / height) * 2 + 1;

  raycaster.setFromCamera(mouse, camera);

  const geometries = scene.children.filter((model) => {
    if (model.type !== "Mesh") return false;
    const mesh = model as THREE.Mesh;
    const material = mesh.material as THREE.Material;

    return material.visible && mesh.userData.name;
  });

  let intersectedModel: SelectedObject = undefined;

  if (many) {
    const intersects = raycaster.intersectObjects(geometries, true);

    if (intersects.length > 0) {
      intersectedModel = intersects[0].object.userData as NonNullable<SelectedObject>;
    }
  } else {
    for (const geometry of geometries) {
      const intersects = raycaster.intersectObject(geometry, true);
      if (intersects.length > 0) {
        intersectedModel = intersects[0].object.userData as NonNullable<SelectedObject>;
        break;
      }
    }
  }

  return intersectedModel;
}
