import { PartitionType, TreeNode } from "#/types/app.types";
import { uid } from "#/utils/uid";

export const setModule = (partition: PartitionType): TreeNode[] => {
  const numberOfModules = 64;
  const modules: TreeNode[] = [];

  for (let i = 1; i <= numberOfModules; i++) {
    const name = partition + i.toString().padStart(2, "0");

    const module: TreeNode = {
      id: uid(),
      state: "notLoaded",
      name,
      nodeEnd: i === numberOfModules,
      partition,
      isModule: true,
    };

    modules.push(module);
  }

  return modules;
};
