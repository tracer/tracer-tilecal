import {
  ParameterPerCellObject,
  ParameterPerChannelMinMax,
  ParameterPerChannelObject,
} from "#/types/app.types";

export function createParameterPerChannelObject(): ParameterPerChannelObject {
  const myObject: ParameterPerChannelObject = {};
  // Outer loop for the first set of keys (LBA, LBC, EBA, EBC)
  const outerKeys = ["LBA", "LBC", "EBA", "EBC"];
  for (const outerKey of outerKeys) {
    myObject[outerKey] = {};

    for (let i = 1; i <= 64; i++) {
      const middleKey = i.toString().padStart(2, "0");
      myObject[outerKey][middleKey] = {};

      for (let j = 0; j <= 47; j++) {
        const innerKey = j.toString();
        myObject[outerKey][middleKey][innerKey] = { 0: [], 1: [] };
      }
    }
  }
  return myObject;
}
export function createParameterPerCellObject(): ParameterPerCellObject {
  const myObject: ParameterPerCellObject = {};
  // Outer loop for the first set of keys (LBA, LBC, EBA, EBC)
  const outerKeys = ["LBA", "LBC", "EBA", "EBC"];
  for (const outerKey of outerKeys) {
    myObject[outerKey] = {};

    for (let i = 1; i <= 64; i++) {
      const middleKey = i.toString().padStart(2, "0");
      myObject[outerKey][middleKey] = {};
    }
  }
  return myObject;
}

export function fillParameterPerChannelObject(data: string): {
  parameterPerChannel: ParameterPerChannelObject;
  parameterPerChannelMinMax: ParameterPerChannelMinMax;
} {
  const result: ParameterPerChannelObject = createParameterPerChannelObject();
  const minMax: ParameterPerChannelMinMax = { 0: {}, 1: {} };

  const lines = data.split("\n").filter(Boolean);
  lines.forEach((line: string) => {
    const splitLines = line.split(/\s+/);
    const sectionName = splitLines[0].slice(0, 3) as keyof ParameterPerChannelObject;
    const moduleNumber = splitLines[0].slice(3);
    result[sectionName][moduleNumber][splitLines[1]][splitLines[2]] = splitLines
      .slice(3)
      .map((value, index) => {
        if (minMax[splitLines[2]][index]) {
          const currentMinValue = minMax[splitLines[2]][index]["min"];
          const currentMaxValue = minMax[splitLines[2]][index]["max"];

          minMax[splitLines[2]][index]["min"] =
            Number(value) < currentMinValue ? Number(value) : currentMinValue;
          minMax[splitLines[2]][index]["max"] =
            Number(value) > currentMaxValue ? Number(value) : currentMaxValue;
        } else {
          minMax[splitLines[2]][index] = {
            min: Number(value),
            max: Number(value),
          };
        }

        return Number(value);
      });
  });
  return { parameterPerChannel: result, parameterPerChannelMinMax: minMax };
}

export function fillNoisePerCellObject(data: string): {
  parameterPerCell: ParameterPerCellObject;
  parameterPerCellMinMax: ParameterPerChannelMinMax;
} {
  const result: ParameterPerCellObject = createParameterPerCellObject();
  const minMax: ParameterPerChannelMinMax = {};

  const lines = data.split("\n").filter(Boolean);

  const gainIndex = 5;
  const maxGain = 4;

  lines.forEach((line: string) => {
    const splitLines = line.split(/\s+/).filter((value) => value !== "");

    let sectionName = splitLines[0].slice(0, 3) as string;
    const moduleNumber = splitLines[0].slice(3);
    let cellName = splitLines[1];
    cellName = splitLines[1].replace("+", "");
    cellName = cellName.replace("sp", "").replace("*", "").replace("mb", "").replace("e4", "");
    if (cellName.includes("B") && !cellName.includes("9") && !["EBA", "EBC"].includes(sectionName))
      cellName = cellName.replace("B", "BC");
    if (cellName.includes("D0")) sectionName = "LBC";
    if (!result[sectionName][moduleNumber][cellName])
      result[sectionName][moduleNumber][cellName] = {};

    result[sectionName][moduleNumber][cellName][splitLines[gainIndex]] = {};
    const constantsData = splitLines.slice(gainIndex + 1);
    for (let i = 0; i < constantsData.length; i += 2) {
      result[sectionName][moduleNumber][cellName][splitLines[gainIndex]][constantsData[i]] = Number(
        constantsData[i + 1]
      );
      if (!minMax[constantsData[i]]) {
        minMax[constantsData[i]] = Object.fromEntries(
          Array.from({ length: maxGain }, (_, index) => [
            index,
            { min: Number(constantsData[i + 1]), max: Number(constantsData[i + 1]) },
          ])
        );
      } else {
        if (minMax[constantsData[i]][splitLines[gainIndex]]["min"] > Number(constantsData[i + 1])) {
          minMax[constantsData[i]][splitLines[gainIndex]]["min"] = +constantsData[i + 1];
        }
        if (minMax[constantsData[i]][splitLines[gainIndex]]["max"] < Number(constantsData[i + 1])) {
          minMax[constantsData[i]][splitLines[gainIndex]]["max"] = +constantsData[i + 1];
        }
      }
    }
  });
  return { parameterPerCell: result, parameterPerCellMinMax: minMax };
}
